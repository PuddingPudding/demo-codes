#import <UIKit/UIKit.h>

#import <UnityFramework/UnityFramework.h>
#import <UnityFramework/NativeCallForUnity.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate , UnityFrameworkListener , NativeCallsProtocol>

//一般變數宣告
@property UnityFramework* ufw;//用來呼叫Unity的指標
@property (strong, nonatomic) UIWindow *nativeWindow;//原生iOS本身的視窗
//(strong代表著該指標指向的記憶體不可被直接釋放，nonatomic則代表存取該變數時不會先做一些安全判定，較省效能）
@property (nonatomic, strong) UIViewController *viewController;//視窗邏輯與設定的類別
@property (nonatomic, strong) UINavigationController *navVC; //iOS中用來管理視窗切換的控制器

//UI變數的宣告（用在Unity開啟後）
@property (nonatomic, strong) UIButton *unloadUnityBtn;
@property (nonatomic, strong) UIButton *hideUnityButton;

//定義在UnityFrameworkListener介面底下的函式，用以註冊Unity的事件聆聽
- (void)unityDidUnload:(NSNotification*)notification;
- (void)unityDidQuit:(NSNotification*)notification;

//定義在NativeCallsProtocol底下的函式
- (void) receiveMessageFromUnity:(char *)msg;
- (char *) getMessageFromiOS; //回傳iOS這裡的字串(預計由Unity接收)
- (void) turnOffUnity;//關閉Unity的方法

//自己再額外定義的函式
- (void)initUnity;
- (void)showUnity;

@end