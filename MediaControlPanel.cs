﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UIControl
{
    public interface ISceneLoader
    {
        void BackToHome();
    }
    public interface IMovementController
    {
        bool IsJoystickMode { get; set; } //需要能夠一直得知現在是否為搖桿狀態才好維持控制面板的按鈕顏色
        /// <summary>
        /// 現在是否碰到了邊界
        /// </summary>
        bool IsTouchingWall { get; } //需要知道有沒有碰到四周以顯示碰壁警示

        void ResetTransform();
    }
    public interface ICrossPointManager
    {
        /// <summary>
        /// 是否正在顯示穿越點
        /// </summary>
        bool IsShowingCrossPoint { get; set; }
        /// <summary>
        /// 是否可穿越
        /// </summary>
        bool IsCrossPointEnable { get; set; } //2021/03/18 感覺應該要將CrossPoint相關的介面抽離，Movement就單純管操作方式並提供重置方法
    }
    public interface IPanoramaMedia
    {
        bool IsPlaying { get; set; }
        PanoramicMediaType MediaType { get; }
        /// <summary>
        /// 當前媒體的總時間 (秒)
        /// </summary>
        float TotalMediaTime { get; }
        /// <summary>
        /// 當前媒體正在播的時間 (秒)
        /// </summary>
        float CurrentMediaTime { get; }
        bool IsVRMode { get; set; }
        bool IsVRModeEnable { get; }

        void SetMediaProgress(float progress);
    }

    public abstract class MediaControlPanel : MonoBehaviour
    {
        protected IMovementController _movementControl;
        protected IPanoramaMedia _panoramaMedia;
        protected ISceneLoader _sceneLoader;
        protected ICrossPointManager _crossPointManager;

        public bool IsInitiated { get; protected set; }

        public virtual void SetMovementControl(IMovementController controller)
        {
            this._movementControl = controller;
        }
        public virtual void SetPanoramaMedia(IPanoramaMedia media)
        {
            this._panoramaMedia = media;
        }
        public virtual void SetSceneLoader(ISceneLoader loader)
        {
            this._sceneLoader = loader;
        }
        public virtual void SetCrossPointManager(ICrossPointManager manager)
        {
            this._crossPointManager = manager;
        }
        public virtual void InitUIEvents()
        {
            this.IsInitiated = true;
        }
        public virtual void UnregisterUIEvents()
        {
            this.IsInitiated = false;
        }

        public abstract void UpdateByControllerInfo(IMovementController controller);

        public abstract void UpdateByCrossPointsInfo(ICrossPointManager manager);

        public abstract void UpdateByMediaInfo(IPanoramaMedia media);

    }
}