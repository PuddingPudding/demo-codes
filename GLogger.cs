﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[Flags]
public enum PlayServiceError : byte
{
    None = 0,
    Timeout = 1,
    NotAuthenticated = 2,
    SaveGameNotEnabled = 4,
    CloudSaveNameNotSet = 8
}

public class GLogger : MonoBehaviour, IProgress
{
    public float Progress { get; protected set; }
    public bool HasQuit { get; protected set; }

    /// <summary>
    /// 資料該從哪讀取 (例如Read cache or Network)
    /// </summary>
    [SerializeField]
    private DataSource m_dataSrc;
    /// <summary>
    /// 資料衝突時以哪邊的資料為準則
    /// </summary>
    [SerializeField]
    [Header("資料衝突時以哪邊的資料為準則")]
    private ConflictResolutionStrategy m_conflictStrategy;

    private bool m_bIsSaving;
    private bool m_bIsCloudDataLoaded = false;

    /// <summary>
    /// 雲端存檔的檔案名稱
    /// </summary>
    private readonly string m_sAcDataFileName = "PlayerAccountData.ss";

    private BinaryFormatter m_formatter;
    /// <summary>
    /// 存檔後呼叫 (主要由外人來註冊)
    /// </summary>
    public Action<SavedGameRequestStatus> OnSave;
    /// <summary>
    /// 讀檔後呼叫 (主要由外人來註冊)
    /// </summary>
    public Action<SavedGameRequestStatus> OnLoad;
    public static GLogger Instance { protected set; get; }

    /// <summary>
    /// 確認現在GooglePlay這邊登入了沒有
    /// </summary>
    public bool HasLogin { get { return Social.localUser.authenticated; } }
    /// <summary>
    /// 登入/登出的callback
    /// </summary>
    public Action<GLogger> LoginCallback;
       
    private void Awake()
    {
        if (GLogger.Instance == null)
        {
            GLogger.Instance = this;
            DontDestroyOnLoad(this.gameObject);
            //在確認了載入完成後就會把changeCallback註冊進去
            this.OnLoad += ((SavedGameRequestStatus status) =>
            {
                AccountDataMgr.ChangeCallback += this.OnAccountDataChange;
            });
            SceneManager.sceneLoaded += this.OnSceneLoaded;
        }
        else
        {
            GLogger glTemp = GLogger.Instance;
            Destroy(this.gameObject);
        }
    }
    private void OnApplicationQuit()
    {
        this.SaveToCloud();
    }

    #region 帳戶資料改變時的callback
    private void OnAccountDataChange(AccountData _data)
    {
        //2020/1/16 改為帳戶資料改變時存檔(因此拿掉原本的存檔動作)
        AccountDataMgr.SetLastSaveTime(DateTime.Now , false);
        this.SaveToCloud();
    }
    #endregion 帳戶資料改變時的callback

    // Use this for initialization
    void Start()
    {
        Debug.Log("GLogger先測試型別" + Social.Active.GetType());
        m_formatter = new BinaryFormatter();

        this.ActiveGooglePlayPlatform(); //設定並啟動GooglePlay平台
        this.GPLogIn(() => { this.LoadFromCloud(); });

        this.OnSceneLoaded(SceneManager.GetActiveScene());
        //匯出的APK檔似乎剛載入不會呼叫OnSceneLoad，因此直接先自己呼叫
    }
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode = LoadSceneMode.Single)
    {
        if (scene.name == ESceneName.TITLE) { this.gameObject.SetActive(true); }
        else { this.gameObject.SetActive(false); }
    }

    /// <summary>
    /// 設定並啟動GooglePlay平台
    /// </summary>
    void ActiveGooglePlayPlatform()
    {
#if UNITY_ANDROID
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
        .EnableSavedGames() /* enables saving game progress. */
        .Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;

        PlayGamesPlatform.Activate();
#endif
    }
    /// <summary>
    /// 嘗試進行GooglePlay登入
    /// </summary>
    /// <param name="successCallback"></param>
    /// <param name="errorCallback"></param>
    public void GPLogIn(Action successCallback = null, Action errorCallback = null)
    {
        try
        {
            Social.localUser.Authenticate((bool success) =>
            {//嘗試登入
                if (success)
                {
                    successCallback?.Invoke();
                    this.LoginCallback?.Invoke(this);
                }
                else
                {
                    errorCallback?.Invoke();
                    this.LoginCallback?.Invoke(this);
                }
            });
        }
        catch (Exception e)
        {
            GLogger.WriteGooglePlayLog("未成功進入Google Play Games Services. exception:" + e.ToString());
            errorCallback?.Invoke();
        }
    }
    public void GPlogOut()
    {
        GLogger.GPSaveThenLogout();
    }

    /// <summary>
    /// 玩家獲得金錢 (只在有登入時去更新帳戶資料)
    /// </summary>
    /// <param name="_iNum"></param>
    public void GPplayerGetCoinCloud(int _iNum)
    {
        if (GLogger.Instance.HasLogin)
        {
            AccountDataMgr.LocalDataGetCoin(_iNum);
        }
        else
        {
            GLogger.WriteGooglePlayLog("離線模式中不計算金錢");
        }
    }

    #region 嘗試GooglePlay帳戶存取
    /// <summary>
    /// 存檔到雲端
    /// </summary>
    public void SaveToCloud()
    {
        try { this.OpenCloudSave(this.OnSaveResponse); }
        catch (Exception e) { GLogger.WriteGooglePlayLog("OpenCloudSave之錯--" + e.ToString()); }
    }
    /// <summary>
    /// 雲端開檔 (基本上無論是要存還是要下載，都需要先經過這個手續)
    /// </summary>
    /// <param name="callback"></param>
    /// <param name="errorCallback"></param>
    public void OpenCloudSave(Action<SavedGameRequestStatus, ISavedGameMetadata> callback
        , Action<PlayServiceError> errorCallback = null)
    {
#region PlayServiceError偵測
        PlayServiceError error = GLogger.CheckPlayServiceError(m_sAcDataFileName);
        if (error != global::PlayServiceError.None)
        {
            errorCallback?.Invoke(error);
        }
        #endregion PlayServiceError偵測
#if UNITY_ANDROID
        PlayGamesPlatform platform = (PlayGamesPlatform)Social.Active; //取得當前社交平台 (我自己是猜說應該代表開啟你的Google帳號)
        try
        {
            platform.SavedGame.OpenWithAutomaticConflictResolution(m_sAcDataFileName
                , m_dataSrc, m_conflictStrategy, callback);
        }
        catch (Exception e) { GLogger.WriteGooglePlayLog("platform.SavedGame.OpenWith~之錯誤" + e.ToString()); }
#endif
    }

    private void OnSaveResponse(SavedGameRequestStatus _status
        , ISavedGameMetadata _meta)
    {
        if (_status == SavedGameRequestStatus.Success)
        {
            byte[] data = null;
            try { data = MyToolFunctions.SerializeData(AccountDataMgr.LocalData); }
            catch (Exception e) { GLogger.WriteGooglePlayLog("資料序列化錯誤" + e.ToString()); }

            SavedGameMetadataUpdate update = new SavedGameMetadataUpdate.Builder()
                .WithUpdatedDescription("內測之正式版格式存檔(時間) :" + DateTime.Now.ToString()) //存檔額外夾帶訊息
                .Build();
#if UNITY_ANDROID
            PlayGamesPlatform platform = (PlayGamesPlatform)Social.Active;
            try { platform.SavedGame.CommitUpdate(_meta, update, data, this.SaveAcDataCallback); }
            catch (Exception e) { GLogger.WriteGooglePlayLog("提交存檔錯誤: " + e.ToString()); }
# endif
        }
        else
        {
            this.OnSave?.Invoke(_status);
            GLogger.WriteGooglePlayLog("試圖Save，但雲端開檔失敗，錯誤為" + _status);
        }
    }

    private void SaveAcDataCallback(SavedGameRequestStatus _status, ISavedGameMetadata _meta)
    {
        this.OnSave?.Invoke(_status);
    }

    public void LoadFromCloud()
    {
        this.Progress = 0; //剛開始開檔的時候，先把進度設為0，後面直接在Callback處設為1
        this.HasQuit = false;
        LoadingManager.Instance.AddLoadingTarget(this);
        this.OpenCloudSave(this.OnLoadResponse);
    }
    /// <summary>
    /// 載入動作，在確認開檔後呼叫
    /// </summary>
    /// <param name="_status"></param>
    /// <param name="_meta"></param>
    private void OnLoadResponse(SavedGameRequestStatus _status, ISavedGameMetadata _meta)
    {
        this.Progress = 1;
        if (_status == SavedGameRequestStatus.Success)
        {
#if UNITY_ANDROID
            PlayGamesPlatform platform = (PlayGamesPlatform)Social.Active;
            try { platform.SavedGame.ReadBinaryData(_meta, this.LoadCallback); }
            catch (Exception e) { GLogger.WriteGooglePlayLog("雲端資料讀取錯誤: " + e.ToString()); }
#endif
        }
        else
        {
            LoadingManager.Instance.DeployErrorWindow(this.LoadErrorRetryCallback
                , "Google Play Error!", "Retry", "Play as local mode");
            AccountDataMgr.SetLocalDataToDefault();
            GLogger.WriteGooglePlayLog("OnLoadResponse錯誤" + _status + "，本地端資料使用預設值");
        }
    }
    private void LoadCallback(SavedGameRequestStatus _status, byte[] _data)
    {//已經成功了才會進來
        try { AccountDataMgr.LocalData = MyToolFunctions.DeserializeData<AccountData>(_data); }
        catch (Exception e) { GLogger.WriteGooglePlayLog("資料反序列化失敗: " + e.ToString()); }
        this.OnLoad?.Invoke(_status);
    }
    private void LoadErrorRetryCallback(bool _bRetry)
    {
        if (_bRetry)
        {
            if (this.HasLogin) { this.LoadFromCloud(); }
            else
            {//尚未登入就再嘗試登入一次，並在登入成功後重新試著載入
                this.GPLogIn(() => { this.LoadFromCloud(); });
            }
        }
        else { GLogger.SimpleLogout(); }
    }
    /// <summary>
    /// 帶入tower類型購買，基本上要在有連線的狀態下才能使用
    /// </summary>
    /// <param name="_towerType"></param>
    public void BuyTower(ETowerType _towerType)
    {
        if (Application.platform == RuntimePlatform.WindowsEditor) { AccountDataMgr.SetLocalTowersOwn(_towerType); }
        else if (GLogger.Instance.HasLogin
            && AccountDataMgr.LocalData.PlayerCoin >= this.GetTowerPrice(_towerType))
        {
            AccountDataMgr.LocalDataGetCoin(-this.GetTowerPrice(_towerType));
            this.UnlockTower(_towerType);
        }
    }
    /// <summary>
    /// 單純地解鎖防禦塔
    /// </summary>
    /// <param name="_towerType"></param>
    public void UnlockTower(ETowerType _towerType)
    {
        if (Application.platform == RuntimePlatform.WindowsEditor) { AccountDataMgr.SetLocalTowersOwn(_towerType); }
        else if (GLogger.Instance.HasLogin)
        {
            AccountDataMgr.SetLocalTowersOwn(_towerType);
        }
    }
#endregion 嘗試GooglePlay帳戶存取


#region 文件中的範例
    /// <summary>
    /// 顯示存檔的視窗
    /// </summary>
    public void ShowSelectUI()
    {
        uint maxNumToDisplay = 5;
        bool allowCreateNew = false;
        bool allowDelete = true;
#if UNITY_ANDROID
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.ShowSelectSavedGameUI("Select saved game (Internal Test)",
            maxNumToDisplay,
            allowCreateNew,
            allowDelete,
            OnSavedGameSelected);
#endif
    }
    public void OnSavedGameSelected(SelectUIStatus status, ISavedGameMetadata game)
    {
        if (status == SelectUIStatus.SavedGameSelected)
        {
            // handle selected game save
        }
        else
        {
            // handle cancel or error
        }
    }

    public void OnSavedGameOpened(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            // handle reading or writing of saved game.
        }
        else
        {
            // handle error
        }
    }
#endregion 文件中的範例

    /// <summary>
    /// 登入後載入資料
    /// </summary>
    public static void GPLoginThenLoad()
    {
        GLogger.Instance.GPLogIn(() => { GLogger.Instance.LoadFromCloud(); });
    }
    /// <summary>
    /// 一般按鈕登出，先存檔再登出
    /// </summary>
    public static void GPSaveThenLogout()
    {
        GLogger.Instance.OnSave += LogoutAfterSave;
        GLogger.Instance.Progress = 0; //剛開始開檔的時候，先把進度設為0，後面直接在Callback處設為1
        GLogger.Instance.HasQuit = false;
        LoadingManager.Instance.AddLoadingTarget(GLogger.Instance);
        GLogger.Instance.SaveToCloud(); //登出前先存個檔
    }
    private static void LogoutAfterSave(SavedGameRequestStatus _status)
    {
        GLogger.SimpleLogout();
        GLogger.Instance.Progress = 1;
        GLogger.Instance.OnSave -= LogoutAfterSave;
        GLogger.Instance.LoginCallback?.Invoke(GLogger.Instance);
    }
    /// <summary>
    /// 單純登出(不存檔)
    /// </summary>
    private static void SimpleLogout()
    {
#if UNITY_ANDROID
        try { PlayGamesPlatform.Instance.SignOut(); }
        catch (Exception e) { WriteGooglePlayLog("登出錯誤:" + e.ToString()); }
        AccountDataMgr.SetLocalDataToDefault();
        AccountDataMgr.ChangeCallback -= GLogger.Instance.OnAccountDataChange;
        //嘗試在登出後把帳戶資料的觀察者拿掉
#endif
    }

    public static void PostToLeaderboard(int _iNewScore, ETowerType _towerType)
    {
        string sBoard = "";
        switch (_towerType)
        {
            case ETowerType.THE_TOWER:
                sBoard = GPGSIds.leaderboard_thetower_highscore;
                break;
            case ETowerType.OCELOT_TOWER:
                sBoard = GPGSIds.leaderboard_ocelottower_highscore;
                break;
            case ETowerType.WESTERN_TOWER:
                sBoard = GPGSIds.leaderboard_westerntower_highscore;
                break;
        }
        Social.ReportScore(_iNewScore, sBoard, (bool success) =>
        {
            if (success) { GLogger.WriteGooglePlayLog("上傳新成績到" + _towerType + "排行榜，分數為" + _iNewScore); } //怪哉，版本12是有顯示這一行的，所以表示上傳成功
            else { GLogger.WriteGooglePlayLog("無法上傳新成績到" + _towerType + "排行榜"); }
        });
    }
    public static void PlayerGainCoin(int _iCoinsGain)
    {
        GLogger.Instance.GPplayerGetCoinCloud(_iCoinsGain);
    }

    /// <summary>
    /// 帶入防禦塔類型並顯示對應的排行榜
    /// </summary>
    /// <param name="_towerType"></param>
    public static void ShowLeaderboardUI(ETowerType _towerType)
    {
#if UNITY_ANDROID
        string sBoard = "";
        switch (_towerType)
        {
            case ETowerType.THE_TOWER:
                sBoard = GPGSIds.leaderboard_thetower_highscore;
                break;
            case ETowerType.OCELOT_TOWER:
                sBoard = GPGSIds.leaderboard_ocelottower_highscore;
                break;
            case ETowerType.WESTERN_TOWER:
                sBoard = GPGSIds.leaderboard_westerntower_highscore;
                break;
        }

        try { PlayGamesPlatform.Instance.ShowLeaderboardUI(sBoard); }
        catch (Exception e) { WriteGooglePlayLog("排行榜開啟錯誤" + e.ToString()); }

        try
        {
            PlayGamesPlatform.Instance.LoadScores(
               sBoard
               , LeaderboardStart.PlayerCentered
               , 1
               , LeaderboardCollection.Public
               , LeaderboardTimeSpan.AllTime
               , (LeaderboardScoreData data) =>
                  {
                      if (!data.Valid)
                      { GLogger.WriteGooglePlayLog("Leaderboard data valid: " + data.Valid); }
                  });
        }
        catch (Exception e) { GLogger.WriteGooglePlayLog("嘗試顯示排行榜之錯誤:" + e.ToString()); }
#endif
    }

    private static void WriteGooglePlayLog(string _str)
    {
        _str = DateTime.Now + " " + _str + "\n";
        string googlePlayLogPath = AppDataPath.GetDataPath(Application.platform) + "/Google Play Log.txt";
        File.AppendAllText(googlePlayLogPath, _str);
    }

#region 處理成就
    public static void UnlockAchievement(string _sID)
    {
        Social.ReportProgress(_sID, 100, success => { });
    }
    public static void IncremntAchievement(string _sID, int _iProgress)
    {
#if UNITY_ANDROID
        PlayGamesPlatform.Instance.IncrementAchievement(_sID, _iProgress, success => { });
#endif
    }
    public static void ShowAchievementsUI()
    {
        Social.ShowAchievementsUI();
    }
#endregion /處理成就

    public int GetTowerPrice(ETowerType _towerType)
    {
        int iPrice = 0;
        switch (_towerType)
        {
            case ETowerType.OCELOT_TOWER:
                iPrice = 12000;
                break;
            case ETowerType.WESTERN_TOWER:
                iPrice = 12000;
                break;
            default:
                iPrice = 0;
                break;
        }
        return iPrice;
    }
    /// <summary>
    /// 取得現在玩家(Local User)的名字，未連結會回傳Unconnected
    /// </summary>
    public string GetPlayerName()
    {
        string sName = "Unconnected";
        if (this.HasLogin) { sName = Social.localUser.userName; }
        return sName;
    }

#region stackoverflow上找到的存檔刪除法
    void DeleteSavedGame(string filename)
    {
#if UNITY_ANDROID
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.OpenWithAutomaticConflictResolution(filename, DataSource.ReadCacheOrNetwork,
            ConflictResolutionStrategy.UseLongestPlaytime, OnDeleteSavedGame);
#endif
    }
    public void OnDeleteSavedGame(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
#if UNITY_ANDROID
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        if (status == SavedGameRequestStatus.Success)
        {
            // delete the game.
            savedGameClient.Delete(game);
        }
        else
        {
            // handle error
        }
#endif
    }
#endregion stackoverflow上找到的存檔刪除法

    public static PlayServiceError CheckPlayServiceError(string _sFileName)
    {
        PlayServiceError error = global::PlayServiceError.None; //(global可用於存取別的命名空間中的變數)
        if (!Social.localUser.authenticated)
        {
            error |= global::PlayServiceError.NotAuthenticated;
            GLogger.WriteGooglePlayLog("Play Service Error-未驗證錯誤");
        }
#if UNITY_ANDROID
        if (PlayGamesClientConfiguration.DefaultConfiguration.EnableSavedGames)
        {
            error |= global::PlayServiceError.SaveGameNotEnabled;
            GLogger.WriteGooglePlayLog("Play Service Error-不可存檔錯誤");
        }
#endif
        if (string.IsNullOrWhiteSpace(_sFileName))
        {
            error |= global::PlayServiceError.CloudSaveNameNotSet;
            GLogger.WriteGooglePlayLog("Play Service Error-雲端存檔之名稱未設定");
        }
        return error;
    }

}
