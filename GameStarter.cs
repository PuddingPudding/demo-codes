﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EGameMode
{
    TUTORIAL,
    NORMAL
}

public class GameStarter : MonoBehaviour
{
    [SerializeField]
    private UIManager m_uiMgr;
    [SerializeField]
    private TowerBank m_twrBank;
    [SerializeField]
    private GameOverManager m_GOM;
    [SerializeField]
    private EGameMode m_gameMode = EGameMode.NORMAL;
    [SerializeField]
    [Header("直接決定要用哪個砲台，測試中使用，未來要讀取玩家的存檔資料")]
    private ETowerType m_towerType;

    private TowerMainScript m_mainPlayer;

    private void Start()
    {
        this.StartGame(AccountDataMgr.LocalData.NowTower, m_gameMode);
    }

    public void StartGame(ETowerType _towerType , EGameMode _gameMode)
    {
        switch(_gameMode)
        {
            case EGameMode.NORMAL:
                this.InitMainPlayerSettings(_towerType);
                break;
            case EGameMode.TUTORIAL:
                this.InitMainPlayerSettings(ETowerType.THE_TOWER);
                TutorialMgr.Instance.SetMainPlayerAndStart(m_mainPlayer);
                break;
        }
    }
    /// <summary>
    /// 初始化砲台對應的基本設定 (血量與攻擊UI等等)
    /// </summary>
    /// <param name="_towerType"></param>
    private void InitMainPlayerSettings(ETowerType _towerType)
    {
        m_mainPlayer = m_twrBank.GetTower(_towerType);
        m_mainPlayer.transform.position = Vector3.zero;
        m_mainPlayer.Init();
        this.m_uiMgr.SetMainPlayer(m_mainPlayer);
        this.m_GOM.SetMainPlayer(m_mainPlayer);
        TextManager.Instance.SetTextTower(_towerType);
        NPCSpawner.Instance.SetMainPlayer(m_mainPlayer);
        MenuManager.Instance.SetMainPlayer(m_mainPlayer);
    }
}
