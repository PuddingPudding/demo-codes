// [!] important set UnityFramework in Target Membership for this file
// [!]           and set Public header visibility

#import <Foundation/Foundation.h>

@protocol NativeCallsProtocol //Protocol有點像是其他程式語言中的介面(Interface)，宣告說需要實作的函式
//這裡的NativeCallsProtocol是給原生iOS專案實作的

@optional //@optional底下的未必要實作
- (void) showHostMainWindow:(NSString*)color; //原本官方範例中要求實作的方法，用以切回主畫面，但此專案中沒有要呼叫

@required //required底下的則一定要實作

//- (void) receiveMessageFromUnity:(NSString *)msg; //用來接收Unity那邊傳來的訊息，NSString是Objective-C的字串，加*代表他是字串指標
- (void) receiveMessageFromUnity:(char *)msg; //發現NSString會失敗，因此改用char

//- (NSString *) getMessageFromiOS; //回傳iOS這裡的字串(預計由Unity接收)
- (char *) getMessageFromiOS; //回傳iOS這裡的字串(預計由Unity接收)

- (void) turnOffUnity;//關閉Unity的方法

@end //在Objective-C當中，你每次宣告類別都要在結尾時加上一個@end


__attribute__ ((visibility("default"))) //__atribute__((visibility))是宣告該類別在動態連結中的能見度，寫default基本上代表著你在其他文檔要使用的時候能直接使用
@interface NativeCallMgr : NSObject //interface相當於其他程式語言中的類別(Class)，這裡宣告說NativeCallMgr繼承自NSObject

+(void) registerNativeCaller:(id<NativeCallsProtocol>) aApi; //註冊原生iOS的呼叫者

@end