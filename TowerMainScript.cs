﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class TowerMainScript : MonoBehaviour
{
    [SerializeField]
    private float m_fRotateSpeed = 95;
    /// <summary>
    /// 轉速升級後加快幾度
    /// </summary>
    private float m_fRotSpdUpgAmt = 60;
    [SerializeField]
    protected float m_fAtkSpeed = 2.5f;
    [SerializeField]
    protected int m_iMaxAmmo = 20;
    [SerializeField]
    protected float m_fMaxHP = 10;
    [SerializeField]
    protected float m_fAmmoDmg = 4;
    [SerializeField]
    protected float m_fReloadTime = 2.8f;
    [SerializeField]
    protected GameObject m_turretPart;
    [SerializeField]
    [Header("每次開槍產生的後座力程度，上限100")]
    protected float m_fRecoilDegreePerShot = 30f;
    [SerializeField]
    [Header("每秒後坐力恢復程度")]
    protected float m_fRecoilRestrain = 50f;
    [Header("開槍倒退距離之上限")]
    [SerializeField]
    protected float m_fMaxRecoilDis = 0.6f;
    [Header("每次開槍時放大比例的上限")]
    [SerializeField]
    private float m_fMaxYScale = 1.6f;

    [Header("最大偏移角度")]
    [SerializeField]
    protected float m_fMaxDriftAngle = 45;
    [SerializeField]
    private GameObject m_aimLeft;
    [SerializeField]
    private GameObject m_aimRight;
    [SerializeField]
    protected Transform m_rotatePart;

    public ETowerType TowerType { get; protected set; }
    public static TowerMainScript MainPlayer { get; protected set; }

    protected AudioSource m_audioSrc;

    protected float m_fAtkCtr = 0;
    protected int m_iCurAmmo = 0;
    protected float m_fCurHP = 0;
    protected float m_fReloadCtr = 0;
    protected int m_iCurCharges = 0;
    protected int[] m_iArrChargesNeed = new int[] { 2, 4, 6, 8 };
    protected int[] m_iArrChargesBackup = new int[] { 1, 2, 3, 4 };//可以額外存的大招充能
    protected int m_iChargesNeedLv = 0;
    protected float m_fCurRecoilDegree = 0; //目前的後座力程度 (砲台向後到怎樣的程度)
    protected float m_fCurDriftAngle = 0;//目前偏移的最大程度
    private Vector3 m_originLocalScale;

    //以下分配給按鈕操控去改變的變數
    protected bool m_bRotatingR = false;
    protected bool m_bRotatingL = false;
    protected bool m_bShooting = false;
    protected bool m_bReloading = false;

    private ModifyNotice m_onHPModify;
    private ModifyNotice m_onAmmoModify;
    private ModifyNotice m_onReloadCtrModify;
    private ModifyNotice m_onReloadStateModify;
    private ModifyNotice m_onChargeNumModify;
    private ModifyNotice m_onUltDurationModify;
    private ModifyNotice m_onUltCostModify;

    public virtual float MaxHP { get { return this.m_fMaxHP; } }
    public virtual float HP { get { return this.m_fCurHP; } }
    public virtual int MaxAmmo { get { return this.m_iMaxAmmo; } }
    public virtual int Ammo { get { return this.m_iCurAmmo; } }
    public virtual float ReloadTime { get { return this.m_fReloadTime; } }
    public virtual float ReloadCtr { get { return this.m_fReloadCtr; } }
    public virtual bool IsReloading { get { return this.m_bReloading; } }
    public virtual int ChargesNeed { get { return m_iArrChargesNeed[m_iChargesNeedLv]; } }
    public virtual int CurrentCharges { get { return this.m_iCurCharges; } }
    public virtual int ChargesMaxBackup { get { return m_iArrChargesBackup[m_iChargesNeedLv]; } }
    public virtual float UltDuration { get; protected set; }
    public virtual float UltDurationMax { get; protected set; }
    /// <summary>
    /// 看出Tower有哪些升級選項已經升了
    /// </summary>
    public virtual bool[] UpgradedArray { get; protected set; }

    /// <summary>
    /// 用來讓別人觀測特別的bool(如罐罐Buffing)的通知者
    /// </summary>
    public ObserveTowerSPBool TowerSPBoolNotice;
    /// <summary>
    /// 用來讓別人觀測特別float(如combo value)的通知者
    /// </summary>
    public ObserveTowerSPFloat TowerSPFloatNotice;
    /// <summary>
    /// 用來讓別人觀測特別int(如炸藥數量)的通知者
    /// </summary>
    public ObserveTowerSPInt TowerSPIntNotice;
    /// <summary>
    /// 單純讓別人觀測防禦塔有哪些功能性的變化
    /// </summary>
    public OnTowerSPChange TowerSPchangeNotice;

    //此區是預估 "The Tower" 專屬的變數
    public virtual bool CombShoot { get; }
    public virtual bool AutoSupply { get; }
    //2019/08/07測試新增UI觀測物
    public virtual float ComboOutputValue { get; }
    public virtual float AutoSupplyOutputValue { get; }
    //2019/11/28 增加可提供文檔存取的內插值陣列
    public string[] TxtParams { get; protected set; }
    /// <summary>
    /// U1實際上需要消耗的能量
    /// </summary>
    public virtual int U1ActualChargesNeed { get { return m_iArrChargesNeed[m_iChargesNeedLv]; } }
    /// <summary>
    /// U2實際上需要消耗的能量
    /// </summary>
    public virtual int U2ActualChargesNeed { get { return m_iArrChargesNeed[m_iChargesNeedLv]; } }
    /// <summary>
    /// U1+U2實際上需要消耗的能量(不是所有砲台都有這一項)
    /// </summary>
    public virtual int U2In1ActualChargesNeed { get { return m_iArrChargesNeed[m_iChargesNeedLv]; } }

    #region 幫防禦塔添加狀態處理
    protected List<TowerStatus> m_listStatus = new List<TowerStatus>();
    /// <summary>
    /// 攻擊受阻
    /// </summary>
    public bool AtkJammed = false;
    #endregion 幫防禦塔添加狀態處理

    public virtual void Init()
    {
        m_iCurAmmo = this.m_iMaxAmmo;
        m_fCurHP = this.m_fMaxHP;
        m_iChargesNeedLv = 0;
        m_fCurRecoilDegree = 0;
        m_originLocalScale = m_turretPart.transform.localScale;
        m_fCurDriftAngle = 0;
        m_fReloadCtr = 0;
        this.m_audioSrc = this.GetComponent<AudioSource>();

        //if (TowerMainScript.MainPlayer != null) { Destroy(TowerMainScript.MainPlayer.gameObject); }
        TowerMainScript.MainPlayer = this;

        this.UpgradedArray = new bool[UpgradeOption.UpgLength];
        for (int i = 0; i < this.UpgradedArray.Length; i++) { this.UpgradedArray[i] = false; }

        this.TxtParams = new string[] { "" }; //換成新架構後，由於文檔參數不可為null的關係，所以需要至少給個無字字串
    }

    /// <summary>
    /// TMS基底會去處理左右旋轉以及檢測換彈中時呼叫HandleReload()，還有HandleRecoil()
    /// </summary>
    protected virtual void Update()
    {
        if (this.m_bRotatingR)
        {
            this.RotateTower(ETowerDir.Right);
        }
        if (this.m_bRotatingL)
        {
            this.RotateTower(ETowerDir.Left);
        }

        if (this.m_bReloading)
        {
            this.HandleReload();
        }

        if (this.m_fCurRecoilDegree > 0)
        {
            m_fCurRecoilDegree -= (m_fRecoilRestrain * Time.deltaTime);
            this.HandleRecoil();
        }

        this.HandleStatus();
    }

    /// <summary>
    /// 按下A鍵時，試圖開槍時呼叫
    /// </summary>
    public abstract void TryToShoot();
    /// <summary>
    /// 處理後座力，主要影響砲台貼圖的位置與槍口偏移角度 (但可能有些砲台不受偏移角度影響?)
    /// </summary>
    protected virtual void HandleRecoil()
    {
        if (m_fCurRecoilDegree > 100)
        {
            m_fCurRecoilDegree = 100;
        }
        if (m_fCurRecoilDegree < 0)
        {
            m_fCurRecoilDegree = 0;
        }
        this.m_turretPart.transform.localPosition = new Vector3(-(m_fMaxRecoilDis * (m_fCurRecoilDegree / 100))
            , m_turretPart.transform.localPosition.y
            , m_turretPart.transform.localPosition.z);
        this.m_turretPart.transform.localScale = new Vector3(m_originLocalScale.x
            , m_originLocalScale.y * (((m_fMaxYScale - 1) * (m_fCurRecoilDegree / 100)) + 1)
            , m_originLocalScale.z);
        m_fCurDriftAngle = m_fMaxDriftAngle * (m_fCurRecoilDegree / 100);
        m_aimRight.transform.localEulerAngles = new Vector3(0, 0, -m_fCurDriftAngle);
        m_aimLeft.transform.localEulerAngles = new Vector3(0, 0, m_fCurDriftAngle);
    }
    /// <summary>
    /// 使用玩家的大招1,2號，輸入3號則是雙招齊用
    /// </summary>
    /// <param name="_ultNum"></param>
    public void UseUlt(int _ultNum)
    {
        switch (_ultNum)
        {
            case (1):
                this.UseUlt(EUltimateType.ULT1);
                break;
            case (2):
                this.UseUlt(EUltimateType.ULT2);
                break;
            case (3):
                this.UseUlt(EUltimateType.ULT1AND2);
                break;
            default:
                Debug.LogWarning("無此大招號碼");
                break;
        }
    }

    public abstract void UseUlt(EUltimateType _ultType);

    /// <summary>
    /// 結束Ult2，這function其實主要是用在教學關卡中，所以TheTower以外的塔沒實作應該也還好
    /// </summary>
    public abstract void EndUlt2();
    /// <summary>
    /// 辨別現在能不能射擊
    /// </summary>
    public abstract bool CanShoot { get; }
    /// <summary>
    /// 處理換彈，換彈中時每禎呼叫
    /// </summary>
    protected abstract void HandleReload();

    /// <summary>
    /// 帶入方向以選轉砲台
    /// </summary>
    /// <param name="_towDir"></param>
    public void RotateTower(ETowerDir _towDir)
    {
        //float rotZ = this.transform.eulerAngles.z;
        float rotZ = this.m_rotatePart.localEulerAngles.z;
        //float rotSpeed = m_bRotateUpgraded && m_bFastRotating ? m_fRotateSpeed + m_fRotSpdUpgAmt : m_fRotateSpeed;
        float rotSpeed = m_fRotateSpeed; //2019/07/11先取消了轉速升級
        switch (_towDir)
        {
            case ETowerDir.Left:
                rotZ += rotSpeed * Time.deltaTime;
                break;
            case ETowerDir.Right:
                rotZ -= rotSpeed * Time.deltaTime;
                break;
            default:
                break;
        }
        //this.transform.eulerAngles = new Vector3(0, 0, rotZ);
        this.m_rotatePart.localEulerAngles = new Vector3(0, 0, rotZ);
    }

    public virtual void Hit(float _fDmg)
    {
        this.m_fCurHP -= _fDmg;
        this.InvokeModify(EPlayerModifyType.OnHPModify);
        if (this.m_fCurHP <= 0)
        {
            this.gameObject.SetActive(false);
        }
    }
    public void GainUltCharge(int _iNum)
    {
        this.m_iCurCharges += _iNum;
        if (m_iCurCharges > m_iArrChargesNeed[m_iChargesNeedLv] + m_iArrChargesBackup[m_iChargesNeedLv])
        {
            this.m_iCurCharges = m_iArrChargesNeed[m_iChargesNeedLv] + m_iArrChargesBackup[m_iChargesNeedLv];            
        }
        else if(m_iCurCharges < 0)
        {
            this.m_iCurCharges = 0;
        }
        this.InvokeModify(EPlayerModifyType.OnChargeNumModify);
    }
    public virtual void GainHealth(float _fNum)
    {
        this.m_fCurHP += _fNum;
        if (this.m_fCurHP > m_fMaxHP) { this.m_fCurHP = this.m_fMaxHP; }
        this.InvokeModify(EPlayerModifyType.OnHPModify);
    }
    /// <summary>
    /// 讓玩家獲得子彈，後面的_bUppgerLimit代表是否受上限影響
    /// </summary>
    /// <param name="_iNum"></param>
    /// <param name="_bUpperLimit"></param>
    public virtual void GainAmmo(int _iNum, bool _bUpperLimit = true)
    {
        if (_bUpperLimit)
        {
            if(m_iCurAmmo > m_iMaxAmmo && _iNum > 0) { _iNum = 0; }
            else if (m_iCurAmmo < 0 && _iNum < 0) { _iNum = 0; }
            else if(_iNum >= 0)
            { _iNum = m_iCurAmmo + _iNum <= m_iMaxAmmo ? _iNum : (m_iMaxAmmo - m_iCurAmmo); }
            else
            { _iNum = m_iCurAmmo + _iNum >= 0 ? _iNum : -m_iCurAmmo; }
        }
        this.m_iCurAmmo += _iNum;
        this.InvokeModify(EPlayerModifyType.OnAmmoModify);
    }

    /// <summary>
    /// TMS基底，在升級時會去將升級列表中的對應選項調成true
    /// </summary>
    /// <param name="_iLevel"></param>
    /// <param name="_iNum"></param>
    public virtual void UpgradeSkill(int _iLevel, int _iNum)
    {
        int iUpgTarget = _iNum;
        for (int i = 0; i < _iLevel; i++) { iUpgTarget += UpgradeOption.UpgNumOfLV[i]; }
        this.UpgradedArray[iUpgTarget] = true;
    }

    //以下主要給按鈕觸發用
    public void RotatingRight(bool _bSwitch)
    {
        this.m_bRotatingR = _bSwitch;
    }
    public void RotatingLeft(bool _bSwitch)
    {
        this.m_bRotatingL = _bSwitch;
    }
    public void TryShooting(bool _bSwitch)
    {
        this.m_bShooting = _bSwitch;
    }
    /// <summary>
    /// 開始換彈，按下R按鈕時呼叫 (通常子彈打乾也會)
    /// </summary>
    public virtual void StartReloading()
    {
        if (!this.m_bReloading && this.m_iCurAmmo < this.m_iMaxAmmo)
        {
            this.m_bReloading = true;
            this.InvokeModify(EPlayerModifyType.OnReloadStateModify);
        }
    }

    //以下主要是通知UI
    public void AddModifyNotice(ModifyNotice _listener, EPlayerModifyType _modifyType)
    {
        switch (_modifyType)
        {
            case EPlayerModifyType.OnHPModify:
                this.m_onHPModify += _listener;
                break;
            case EPlayerModifyType.OnAmmoModify:
                this.m_onAmmoModify += _listener;
                break;
            case EPlayerModifyType.OnReloadCtrModify:
                this.m_onReloadCtrModify += _listener;
                break;
            case EPlayerModifyType.OnReloadStateModify:
                this.m_onReloadStateModify += _listener;
                break;
            case EPlayerModifyType.OnChargeNumModify:
                this.m_onChargeNumModify += _listener;
                break;
            case EPlayerModifyType.OnUltDurationModify:
                this.m_onUltDurationModify += _listener;
                break;
            case EPlayerModifyType.OnUltCostModify:
                this.m_onUltCostModify += _listener;
                break;
        }
    }
    public void InvokeModify(EPlayerModifyType _modifyType)
    {
        ModifyNotice mnTemp = null;
        switch (_modifyType)
        {
            case EPlayerModifyType.OnHPModify:
                mnTemp = this.m_onHPModify;
                break;
            case EPlayerModifyType.OnAmmoModify:
                mnTemp = this.m_onAmmoModify;
                break;
            case EPlayerModifyType.OnReloadCtrModify:
                mnTemp = this.m_onReloadCtrModify;
                break;
            case EPlayerModifyType.OnReloadStateModify:
                mnTemp = this.m_onReloadStateModify;
                break;
            case EPlayerModifyType.OnChargeNumModify:
                mnTemp = this.m_onChargeNumModify;
                break;
            case EPlayerModifyType.OnUltDurationModify:
                mnTemp = this.m_onUltDurationModify;
                break;
            case EPlayerModifyType.OnUltCostModify:
                mnTemp = this.m_onUltCostModify;
                break;
        }
        if (mnTemp != null)
        {
            Delegate[] arrCallback = mnTemp.GetInvocationList();
            for (int i = 0; i < arrCallback.Length; i++) //假設今天事件不只一個
            {
                ModifyNotice callbackTemp = (ModifyNotice)arrCallback[i];
                callbackTemp.Invoke();
            }
        }
    }

    /// <summary>
    /// 加入狀態處理
    /// </summary>
    private void HandleStatus()
    {
        for (int i = 0; i < m_listStatus.Count; i++)
        {
            if (this.m_listStatus[i].IsWorking) { this.m_listStatus[i].Invoke(this); }
            else
            {
                TowerStatus statusTemp = this.m_listStatus[i];
                this.m_listStatus.RemoveAt(i);
                i--; //刪掉一個後，索引值需往前一步
                statusTemp.Dispose(); //我其實也不確定這樣子釋放記憶體恰不恰當
            }
        }
    }
    public void AddStatus(TowerStatus _status)
    {
        this.m_listStatus.Add(_status);
        _status.TargetPlayer = this;
    }
    /// <summary>
    /// 去檢查說該防禦塔預設有哪些特殊變化 (主要是給UI觀察用的)
    /// </summary>
    public abstract ESpecialTowerChange[] GetDefaultSPChange();
}