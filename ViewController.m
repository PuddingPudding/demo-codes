#import "ViewController.h"
#import "AppDelegate.h"
#import <UIKit/UIKit.h>

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"視窗生成後呼叫");

    self.view.backgroundColor = [UIColor greenColor];
    AppDelegate* hostDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //sharedApplication會去取得現在這個App本身，然後我們再去取得其中的AppDelegate

    self.unityInitBtn = [UIButton buttonWithType: UIButtonTypeSystem];
    [self.unityInitBtn setTitle: @"初始化Unity" forState: UIControlStateNormal];//設定UI物件的狀態，例如他是否正被選取中
    self.unityInitBtn.frame = CGRectMake(0, 0, 100, 44);
    self.unityInitBtn.center = CGPointMake(100, 120);//設定其位置，上面那一行則宣告大小
    self.unityInitBtn.backgroundColor = [UIColor blueColor];
    [self.unityInitBtn addTarget: hostDelegate action: @selector(initUnity) forControlEvents: UIControlEventPrimaryActionTriggered];
    //按鈕事件，註冊說一但按下就會去呼叫AppDelegate中的initUnity函式
    [self.view addSubview: self.unityInitBtn]; //這一行估計是把UI物件放進畫面

    self.unityShowBtn =  [UIButton buttonWithType: UIButtonTypeSystem];
    [self.unityShowBtn setTitle: @"顯示Unity" forState: UIControlStateNormal];
    self.unityShowBtn.frame = CGRectMake(0, 0, 100, 44);
    self.unityShowBtn.center = CGPointMake(250, 120);
    self.unityShowBtn.backgroundColor = [UIColor lightGrayColor];
    [self.unityShowBtn addTarget: hostDelegate action: @selector(showUnity) forControlEvents: UIControlEventPrimaryActionTriggered];
    [self.view addSubview: self.unityShowBtn];
}


@end