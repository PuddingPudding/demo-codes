﻿Shader "Demo/Scanner Shader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_ScanDistance("Scan Distance" , float) = 0 //當前掃描距離
		_FillColor("Fill Color", Color) = (0, 0, 0, 1)		//被掃描過的區域之顏色
	}
		SubShader
		{
			Tags { "RenderType" = "Opaque" }
			LOD 100

			Cull Off //關閉汰除

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				// make fog work
				#pragma multi_compile_fog

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
					float4 sight : TEXCOORD1; //視線 (該紋理座標交給C#那邊幫忙設定)
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					UNITY_FOG_COORDS(1)
					float4 vertex : SV_POSITION;
					float4 sight : TEXCOORD1;//視線 (從appdata那邊取得設定好的視線向量)
				};

				sampler2D _MainTex;
				float4 _MainTex_ST;

				sampler2D_float  _CameraDepthTexture; //宣告說存取MainCamera的深度紋理(必須打一樣的名字)
				//sampler2D代表2D紋理(可被取樣的)
				//後面加上_float代表採用了較高的精準度(用較多記憶體在表示)

				float4 _FillColor;
				float _ScanDistance;
				float4 _WorldSpaceScannerPos; //掃描特效的源頭位置
				//僅在Pass當中宣告，沒有寫在Properties欄位中，這樣的話就只能靠程式調動，無法直接在編輯畫面裡調整
				float _CamFarClip; //Camera的最遠可視距離

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = TRANSFORM_TEX(v.uv, _MainTex);
					UNITY_TRANSFER_FOG(o,o.vertex);
					o.sight = v.sight; //去取得appdata獲得的視線向量
					return o;
				}

				float4 FillArea(float distance, float scanDistance, float4 col)
				{
					if (distance <= scanDistance)//若前者距離在掃瞄範圍內，回傳特殊顏色
					{
						return col;
					}
					else //否則回傳透明
					{
						return float4(0,0,0,0);
					}
				}
				float4 SquareFill(float3 pos, float scanDisX, float scanDisZ, float4 col) //將位置的X軸和Z軸分開判斷
				{
					float absPosX = abs(pos.x);
					float absPosZ = abs(pos.z);
					if (absPosX < scanDisX && absPosZ < scanDisZ)
					{
						return col;
					}
					else
					{
						return float4(0, 0, 0, 0);
					}
				}

				fixed4 frag(v2f i) : SV_Target
				{
					// sample the texture
					fixed4 col = tex2D(_MainTex, i.uv);

					float depth = tex2D(_CameraDepthTexture, i.uv).x; //取得當前繪製點的深度值，1為近，0為遠 (非線性)
					float linearDepth = Linear01Depth(depth); //改取得0~1的深度值，0為最近，1為Camera可看到的最遠處 (線性)
					//float linearDepth = 1 - depth;  //注意，你不能直接寫1-原深度值，具體原因我也不是很明白

					//float actualDistance = _CamFarClip * linearDepth; //將當前頂點的線性深度(幾%)乘上最遠可視距離，換算出實際上當前頂點到Camera的距離
					//float actualDistance = length(i.sight) * linearDepth; //改為線性深度乘上該點位看向最遠處的向量距離

					float4 posFromCam = i.sight * linearDepth; //改為線性深度乘上該點位看向最遠處的向量

					float3 actualPos = _WorldSpaceCameraPos + posFromCam; //加上Camera的位置就可得出具體的位置

					float3 posFromScanner = actualPos - _WorldSpaceScannerPos; //最後我們要計算具體位置和掃描點的差異 (以玩家為起點，而非Camera)

					//float4 fillClr = FillArea(posFromScanner, _ScanDistance, _FillColor); //舊方法
					float4 fillClr = SquareFill(posFromScanner, _ScanDistance , _ScanDistance , _FillColor);

					float4 clrBlend = float4(1, 1, 1, 1); //混色數值，當還沒掃描過去時，將原色彩乘上白色(1,1,1,1)將獲得原色彩
					clrBlend = lerp(clrBlend, fillClr, fillClr.a);

					col *= clrBlend;

					return col;
				}
				ENDCG
			}
		}
}