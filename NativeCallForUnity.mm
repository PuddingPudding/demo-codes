#import <Foundation/Foundation.h>
#import "NativeCallForUnity.h"


@implementation NativeCallMgr : NSObject //@implementation代表著這一段是實作 (在.h檔那邊看到的只是純宣告)

id<NativeCallsProtocol> api = NULL; //id代表著你不確定他會是什麼型態，但後面<>內表示說該型態必須要有實作NativeCallsProtocol介面

+(void) registerNativeCaller:(id<NativeCallsProtocol>) aApi //方法前面寫+代表靜態方法，這裡做的是註冊NativeCallsProtocol的實作者
{
    api = aApi;
}

@end


extern "C" {//extern "C" 代表以下區段以Ｃ語言解讀
//該區段內要實作給Unity那邊呼叫的方法

    char* GetMessageFromiOS()
    {
        return [api getMessageFromiOS]; //當Unity呼叫NativeiOS.GetMessageFromiOS()時，會來呼叫這裡api的getMessageFromiOS
	//Objective-C當中，[變數 函式名稱]這個寫法相當於其他語言中的變數.函式名稱()
    }

//    void SendMessageToiOS(NSString* msg) //發現NSString會失敗，因此改用char
    void SendMessageToiOS(char *msg)
    {
        return [api receiveMessageFromUnity:msg];
	//receiveMessageFromUnity:後面帶入參數
    }


    void TurnOffUnity()
    {
        return [api turnOffUnity];
    }
}