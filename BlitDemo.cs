﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlitDemo : MonoBehaviour
    {
        [SerializeField]
        private Material m_demoMat;

        [SerializeField]
        private Camera m_cam;

        private void Start()
        {
            m_demoMat.SetFloat("_Brightness", 1f); //剛開啟時將亮度調為一般
            if(m_cam == null) { m_cam = this.GetComponent<Camera>(); }
        }

        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            //Graphics.Blit(source, m_demoMat); //將當前的畫面截圖(source)帶入Material中進行處理
            this.QuadBlit(source, destination, m_demoMat); //改去呼叫我們新寫的Blit函式
        }

        // Update is called once per frame
        void Update()
        {
            float fBrightness = m_demoMat.GetFloat("_Brightness");
            if (Input.GetKeyDown(KeyCode.UpArrow) )
            {
                m_demoMat.SetFloat("_Brightness" , fBrightness + 0.1f);
            }
            else if(Input.GetKeyDown(KeyCode.DownArrow))
            {
                m_demoMat.SetFloat("_Brightness", fBrightness - 0.1f);
            }
        }

        private void QuadBlit(RenderTexture source, RenderTexture dest, Material mat)
        {
            RenderTexture.active = dest; //設定現在被渲染的RenderTexture，這裡設為輸出的那一個
           //RenderTexture.active = null; //你也可以設為null，這麼做的話接下來的渲染會改成去渲染主畫面 (某種程度上跟選擇dest一樣)

            mat.SetTexture("_MainTex", source); //將原本的畫面截圖傳進Shader當中

            GL.PushMatrix(); //儲存模型，投影矩陣等資訊……我也看不太懂
            GL.LoadOrtho(); //帶入正投影(orthograhic projection)矩陣，看不太懂，但是拿掉這一行的話就無法正常顯示

            mat.SetPass(0); //將帶入的材質設為當前繪製用的材質

            GL.Begin(GL.QUADS); //請GL開始繪製四邊形

            GL.MultiTexCoord2(0, 0.0f, 0.0f);//設定Shader當中的第一組紋理座標(TEXCOORD0)，一般來說第一組都是用來表示UV座標
            GL.Vertex3(0.0f, 0.0f, 0.0f); //從0.0(畫面左下角開始繪製)

            GL.MultiTexCoord2(0, 1.0f, 0.0f);
            GL.Vertex3(1.0f, 0.0f, 0.0f);

             GL.MultiTexCoord2(0, 1.0f, 1.0f); 
            GL.Vertex3(1.0f, 1.0f, 0.0f);

            GL.MultiTexCoord2(0, 0.0f, 1.0f);
            GL.Vertex3(0.0f, 1.0f, 0.0f); //一路設定到0,1 (畫面左上角)

            GL.End(); //GL繪製結束
            GL.PopMatrix(); //取出先前儲存的模型與投影矩陣等資訊
        }
    }