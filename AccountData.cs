﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

/// <summary>
/// 將AccountData中的資料部分單獨抽出來，之後再把名字改掉
/// </summary>
[System.Serializable]
public class AccountData
{
    public AccountData()
    {
        this.LastSaveTime = DateTime.Now;
        this.PlayerCoin = 0;
        this.NowTower = ETowerType.THE_TOWER;
        this.TowersOwn = ETowersOwn.THE_TOWER;
        this.LastAdTicketRechargeTime = new DateTime(2020, 1, 15, 0, 0, 0);
        this.DailyRewardTicket = 0;
    }
    /// <summary>
    /// 最後一次存檔時間
    /// </summary>
    public DateTime LastSaveTime { set; get; }
    /// <summary>
    /// 玩家的金幣數量
    /// </summary>
    public int PlayerCoin
    {//玩家金幣數不可低於0
        set { this.m_iPlayerCoin = (value >= 0) ? value : 0; }
        get { return this.m_iPlayerCoin; }
    }
    private int m_iPlayerCoin;

    /// <summary>
    /// 當前準備出擊的Tower
    /// </summary>
    public ETowerType NowTower
    {
        set
        {
            if (((1 << (int)value) & (int)this.TowersOwn) > 0) //取2的X次方，並跟TowersOwn(flags)去做比對，看是否有對中 (逐位元比較) 
            {
                this.m_nowTower = value;
            }
        }
        get { return this.m_nowTower; }
    }
    private ETowerType m_nowTower;

    /// <summary>
    /// 目前該玩家解鎖了哪些Tower
    /// </summary>
    public ETowersOwn TowersOwn { protected set; get; }
    /// <summary>
    /// 最後一次廣告獎勵券開始充能的時間
    /// </summary>
    public DateTime LastAdTicketRechargeTime
    {
        set
        {//只在新的時間晚於上次看完每日獎勵時才會存入
            m_lastAdTicketRechargeTime =
                DateTime.Compare(value, m_lastAdTicketRechargeTime) > 0 ?
                value : m_lastAdTicketRechargeTime;
        }
        get { return this.m_lastAdTicketRechargeTime; }
    }
    private DateTime m_lastAdTicketRechargeTime = DateTime.MinValue;
    /// <summary>
    /// 去看說目前還可以看幾個廣告
    /// </summary>
    public int DailyRewardTicket { get; set; }
    /// <summary>
    /// 設定帳戶資料所擁有的砲台 (後面帶true為獲得，false為失去)
    /// </summary>
    /// <param name="_tower"></param>
    /// <param name="_bGain"></param>
    public void SetTowersOwn(ETowersOwn _tower, bool _bGain = true)
    {
        if (_bGain) { this.TowersOwn |= _tower; }
        else
        {
            if ((this.TowersOwn & _tower) > 0) { this.TowersOwn -= _tower; }
        }
    }
}

/// <summary>
/// 帳號資料管理者 (基本上沒有實體，只有static函式)
/// </summary>
[System.Serializable]
public class AccountDataMgr
{
    /// <summary>
    /// 目前存在此裝置上的本地端帳號資料
    /// </summary>
    public static AccountData LocalData
    {
        get
        {
            if (g_localData == null) { g_localData = new AccountData(); }
            return g_localData;
        }
        set
        {
            g_localData = value;
            //MyToolFunctions.WriteFileLog("AccountData set，通知觀察者");
            ChangeCallback?.Invoke(g_localData);
        }
    }
    private static AccountData g_localData;

    /// <summary>
    /// 帳戶資料變化時會做的呼叫
    /// </summary>
    public static Action<AccountData> ChangeCallback;
    /// <summary>
    /// 一次最多可以儲存多少廣告票券
    /// </summary>
    public static int DailyRewardMax { get { return 5; } }
    /// <summary>
    /// 每過多久可以獲得一張廣告票券 (暫定6小時)
    /// </summary>
    public static TimeSpan AdTicketTime { get { return new TimeSpan(6, 0, 0); } }

    public static void LocalDataGetCoin(int _iNum)
    {
        g_localData.PlayerCoin += _iNum;
        ChangeCallback?.Invoke(g_localData);
    }
    public static void LocalDataSetCoin(int _iNum)
    {
        g_localData.PlayerCoin = _iNum;
        ChangeCallback?.Invoke(g_localData);
    }
    /// <summary>
    /// 設定帳戶資料所擁有的砲台 (後面帶true為獲得，false為失去)
    /// </summary>
    /// <param name="_tower"></param>
    /// <param name="_bGain"></param>
    public static void SetLocalTowersOwn(ETowersOwn _tower, bool _bGain = true)
    {
        g_localData.SetTowersOwn(_tower, _bGain);
        ChangeCallback?.Invoke(g_localData);
    }
    public static void SetLocalTowersOwn(ETowerType _towerType, bool _bGain = true)
    {
        if (_towerType != ETowerType.THE_TOWER)
        {
            ETowersOwn towersOwn = (ETowersOwn)(1 << (int)_towerType);
            g_localData.SetTowersOwn(towersOwn, _bGain);
            ChangeCallback?.Invoke(g_localData);
        }
    }
    /// <summary>
    /// 設定本地帳戶的當前Tower (成功則回傳True，失敗則false)
    /// </summary>
    /// <param name=""></param>
    /// <returns></returns>
    public static bool SetLocalNowTower(ETowerType _towerType)
    {
        g_localData.NowTower = _towerType;
        ChangeCallback?.Invoke(g_localData);
        return (g_localData.NowTower == _towerType); //我想說他本來就會做驗證了
    }
    /// <summary>
    /// 查看玩家是否有解鎖該塔
    /// </summary>
    /// <param name="_towerType"></param>
    /// <returns></returns>
    public static bool CheckIsTowerOwn(ETowerType _towerType)
    {
        return (((int)LocalData.TowersOwn & (1 << (int)_towerType)) > 0);
    }
    public static bool CheckIsTowerOwn(int _iTowerType)
    {
        return (((int)LocalData.TowersOwn & (1 << _iTowerType)) > 0);
    }

    public static void SetLocalDataToDefault()
    {
        g_localData = new AccountData();
        ChangeCallback?.Invoke(g_localData);
    }
    /// <summary>
    /// 不確定設定存檔時間該不該通知觀察者，因為這個數值基本上都用不到 (目前預設不通知)
    /// </summary>
    /// <param name="_dateTime"></param>
    public static void SetLastSaveTime(DateTime _dateTime , bool _bCallback = false)
    {
        g_localData.LastSaveTime = _dateTime;
        if (_bCallback) { ChangeCallback?.Invoke(g_localData); }
    }

    /// <summary>
    /// 去計算廣告券的充能時間，同時依據現在時間去設定現在有幾張票券
    /// </summary>
    /// <returns></returns>
    public static TimeSpan CalculateAdTicketTime()
    {
        TimeSpan ticketTime = new TimeSpan(0, 0, 0);
        if (g_localData.DailyRewardTicket < AccountDataMgr.DailyRewardMax)
        {//只在票券少於最高值的時候去計算
            bool bTicketHasChange = false;
            while (g_localData.LastAdTicketRechargeTime + AccountDataMgr.AdTicketTime <= DateTime.Now
                && g_localData.DailyRewardTicket < AccountDataMgr.DailyRewardMax)
            {
                g_localData.LastAdTicketRechargeTime += AccountDataMgr.AdTicketTime;
                g_localData.DailyRewardTicket++;
                bTicketHasChange = true;
            }
            if(g_localData.LastAdTicketRechargeTime + AccountDataMgr.AdTicketTime > DateTime.Now)
            {
                ticketTime = DateTime.Now - g_localData.LastAdTicketRechargeTime;
            }
            if (bTicketHasChange) { AccountDataMgr.ChangeCallback?.Invoke(g_localData); }
        }
        return ticketTime;
    }
    /// <summary>
    /// 觀看日常獎勵後的變化處理
    /// </summary>
    public static void ChangeAfterWatchingDailyAd()
    {
        if(g_localData.DailyRewardTicket >= AccountDataMgr.DailyRewardMax)
        {
            g_localData.DailyRewardTicket--;
            if(g_localData.DailyRewardTicket < AccountDataMgr.DailyRewardMax)
            {//假如觀看後到票券數低於最大值，那就重新開始計算票券充能時間
                g_localData.LastAdTicketRechargeTime = DateTime.Now;
            }
        }
        else { g_localData.DailyRewardTicket--; }
        ChangeCallback?.Invoke(g_localData);
    }
}