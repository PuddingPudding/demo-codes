using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MobileMsgMgr : MonoBehaviour
{
#if UNITY_ANDROID
        public AndroidJavaObject GetAndroidCurrentActivity()
        {
            return new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
        }//取得Unity在Android系統中的Activity.java
#endif

#if UNITY_IOS
    public class NativeiOS
    {
        //DllImport代表這個函式是從動態函式庫(DLL)中被引入
        //這裡因為iOS插件是直接與執行檔連結的關係，因此要選擇從__Internal這個函式庫引入
        [DllImport("__Internal")]
        public static extern string GetMessageFromiOS(); //extern代表宣告該方法會在外部被實作，不會在該專案內

        [DllImport("__Internal")]
        public static extern void SendMessageToiOS(string msg);

        [DllImport("__Internal")]
        public static extern void TurnOffUnity();
    }
#endif

        [SerializeField]
        private Text _msgFromMobileTxt; //用來顯示Android那裡傳來的訊息

        [SerializeField]
        private Button _sendMsgBtn;

        [SerializeField]
        private Button _exitBtn;

        [SerializeField]
        private InputField _msgField;

        private void Start()
        {
            this._msgFromMobileTxt.text = this.GetMsgFromMobile();
            this._sendMsgBtn.onClick.AddListener(() => { this.SendMsgToMobile(this._msgField.text); });
            this._exitBtn.onClick.AddListener(this.LeaveUnity);
        }//對UI物件做事件註冊

        public void SendMsgToMobile(string msg)
        {
            Debug.Log("Unity這邊的訊息為: " + msg);
#if UNITY_ANDROID && !UNITY_EDITOR
            this.GetAndroidCurrentActivity().Call("getMsgFromUnity" , msg);
            //呼叫AndroidStudio那邊的getMsgFromUnity，就我所知只能傳入字串或整數
#elif UNITY_IOS
            NativeiOS.SendMessageToiOS(msg);
            //呼叫SendMessageToiOS的實作者，將Unity這邊的字串傳過去
#endif
        }

        public string GetMsgFromMobile()
        {
            string msg = "";
#if UNITY_ANDROID && !UNITY_EDITOR
            msg = this.GetAndroidCurrentActivity().Call<string>("sendMsgToUnity");
            //叫Android送出訊息給Unity
#elif UNITY_IOS
            msg = NativeiOS.GetMessageFromiOS();
            //呼叫GetMessageFromiOS的實作者
#endif
            return msg;
        }

        public void LeaveUnity()
        {
            Debug.Log("Try to quit from Unity");
#if UNITY_ANDROID
            Application.Quit(); //在Android當中，要返回原生App只要呼叫Application.Quit()就好
#elif UNITY_IOS
            NativeiOS.TurnOffUnity(); //iOS的話則要看他的實作者決定怎麼執行
#endif
        }

}