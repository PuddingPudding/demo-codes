﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EPrefabType
{
    BULLET,
    BURST_SPARK,
    ENEMY,
    PICKUP,
    CHARGE_UP_PARTICLE,
    COMBO_BONUS_PARTICLE,
    WARHEAD,
    WARNING_EFFECT,
    DRUNKARD,
    GAIN_HP_PARTICLE,
    HP_PICKUP,
    CAN_FOOD_BUFF_PARTICLE,
    SHOCKER,
    POWER_DOWN_PARTICLE,
    RAVAGER
}

public class ObjectPool : MonoBehaviour
{
    [SerializeField]
    private PrefabScript m_bulletPrefab;
    [SerializeField]
    private PrefabScript m_burstSparkPrefab;
    [SerializeField]
    private PrefabScript m_enemyPrefab;
    [SerializeField]
    private PrefabScript m_pickupPrefab;
    [SerializeField]
    private PrefabScript m_chargeUpParticlePrefab;
    [SerializeField]
    private PrefabScript m_warheadPrefab;
    [SerializeField]
    private PrefabScript m_warningEffectPrfab;
    [SerializeField]
    private PrefabScript m_drunkardPrefab;
    [SerializeField]
    private PrefabScript m_healthChargePrefab;
    [SerializeField]
    private PrefabScript m_healthUpParticlePrefab;
    [SerializeField]
    private PrefabScript m_canFoodBuffParticlePrefab;
    [SerializeField]
    private PrefabScript m_shockerPrefab;
    [SerializeField]
    private PrefabScript m_powerDownParticlePrefab;
    [SerializeField]
    private PrefabScript m_ravagerPrefab;

    [SerializeField]
    private List<PrefabScript> m_listBulletPrefab = new List<PrefabScript>();
    [SerializeField]
    private List<PrefabScript> m_listBurstSparkPrefab = new List<PrefabScript>();
    [SerializeField]
    private List<PrefabScript> m_listEnemyPrefab = new List<PrefabScript>();
    [SerializeField]
    private List<PrefabScript> m_listPickupPrefab = new List<PrefabScript>();
    [SerializeField]
    private List<PrefabScript> m_listChargeUpParticlePrefab = new List<PrefabScript>();
    [SerializeField]
    private List<PrefabScript> m_listWarheadPrefab = new List<PrefabScript>();
    [SerializeField]
    private List<PrefabScript> m_listWarningEffectPrfab = new List<PrefabScript>();
    [SerializeField]
    private List<PrefabScript> m_listDrunkardPrefab = new List<PrefabScript>();
    [SerializeField]
    private List<PrefabScript> m_listHealthChargePrefab = new List<PrefabScript>();
    [SerializeField]
    private List<PrefabScript> m_listHealthUpParticlePrefab = new List<PrefabScript>();
    [SerializeField]
    private List<PrefabScript> m_listCanFoodBuffParticlePrefab = new List<PrefabScript>();
    [SerializeField]
    private List<PrefabScript> m_listShockerPrefab = new List<PrefabScript>();
    [SerializeField]
    private List<PrefabScript> m_listPowerDownParticlePrefab = new List<PrefabScript>();
    [SerializeField]
    private List<PrefabScript> m_listRavagerPrefab = new List<PrefabScript>();

    private static ObjectPool g_instance = null;
    private static object g_poolLock = new object();

    private ObjectPool() //建構子宣告私有化，避免別人生成實體
    {
    }
    private void Awake()
    {
        if (ObjectPool.g_instance == null)
        {
            ObjectPool.g_instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            ObjectPool.g_instance.RefreshPool();
            Destroy(this.gameObject);
        }
    }
    public static ObjectPool Instance //唯一存在的getter
    {
        get
        {
            return ObjectPool.g_instance;
        }
    }


    private void RefreshPool()
    {
        this.m_listBulletPrefab.Clear();
        this.m_listBurstSparkPrefab.Clear();
        this.m_listEnemyPrefab.Clear();
        this.m_listChargeUpParticlePrefab.Clear();
        this.m_listPickupPrefab.Clear();
        this.m_listWarheadPrefab.Clear();
        this.m_listWarningEffectPrfab.Clear();
        this.m_listDrunkardPrefab.Clear();
        this.m_listHealthChargePrefab.Clear();
        this.m_listHealthUpParticlePrefab.Clear();
        this.m_listCanFoodBuffParticlePrefab.Clear();
        this.m_listShockerPrefab.Clear();
        this.m_listPowerDownParticlePrefab.Clear();
        this.m_listRavagerPrefab.Clear();
    }

    public PrefabScript GetPrefabScript(EPrefabType _prefabType)
    {
        lock(g_poolLock )
        {
            List<PrefabScript> listPrefabChose = null;
            PrefabScript prefabChose = null;
            PrefabScript prefabTemp = null;
            switch (_prefabType)
            {
                case EPrefabType.BULLET:
                    listPrefabChose = this.m_listBulletPrefab;
                    prefabChose = this.m_bulletPrefab;
                    break;
                case EPrefabType.BURST_SPARK:
                    listPrefabChose = this.m_listBurstSparkPrefab;
                    prefabChose = this.m_burstSparkPrefab;
                    break;
                case EPrefabType.ENEMY:
                    listPrefabChose = this.m_listEnemyPrefab;
                    prefabChose = this.m_enemyPrefab;
                    break;
                case EPrefabType.PICKUP:
                    listPrefabChose = this.m_listPickupPrefab;
                    prefabChose = this.m_pickupPrefab;
                    break;
                case EPrefabType.CHARGE_UP_PARTICLE:
                    listPrefabChose = this.m_listChargeUpParticlePrefab;
                    prefabChose = this.m_chargeUpParticlePrefab;
                    break;
                case EPrefabType.WARHEAD:
                    listPrefabChose = this.m_listWarheadPrefab;
                    prefabChose = this.m_warheadPrefab;
                    break;
                case EPrefabType.WARNING_EFFECT:
                    listPrefabChose = this.m_listWarningEffectPrfab;
                    prefabChose = this.m_warningEffectPrfab;
                    break;
                case EPrefabType.DRUNKARD:
                    listPrefabChose = this.m_listDrunkardPrefab;
                    prefabChose = this.m_drunkardPrefab;
                    break;
                case EPrefabType.HP_PICKUP:
                    listPrefabChose = this.m_listHealthChargePrefab;
                    prefabChose = this.m_healthChargePrefab;
                    break;
                case EPrefabType.GAIN_HP_PARTICLE:
                    listPrefabChose = this.m_listHealthUpParticlePrefab;
                    prefabChose = this.m_healthUpParticlePrefab;
                    break;
                case EPrefabType.CAN_FOOD_BUFF_PARTICLE:
                    listPrefabChose = this.m_listCanFoodBuffParticlePrefab;
                    prefabChose = this.m_canFoodBuffParticlePrefab;
                    break;
                case EPrefabType.SHOCKER:
                    listPrefabChose = this.m_listShockerPrefab;
                    prefabChose = this.m_shockerPrefab;
                    break;
                case EPrefabType.POWER_DOWN_PARTICLE:
                    listPrefabChose = this.m_listPowerDownParticlePrefab;
                    prefabChose = this.m_powerDownParticlePrefab;
                    break;
                case EPrefabType.RAVAGER:
                    listPrefabChose = this.m_listRavagerPrefab;
                    prefabChose = this.m_ravagerPrefab;
                    break;
                default:
                    print("沒有對應的物件: " + _prefabType);
                    break;
            }

            if (listPrefabChose.Count > 0)
            {
                prefabTemp = listPrefabChose[0];
                listPrefabChose.RemoveAt(0);
            }
            else
            {
                prefabTemp = Instantiate(prefabChose.gameObject).GetComponent<PrefabScript>();
            }
            prefabTemp.gameObject.SetActive(true);
            return prefabTemp;
        }        
    }

    public GameObject GetPrefab(EPrefabType _prefabType)
    {
        PrefabScript prefabTemp = this.GetPrefabScript(_prefabType);
        if(prefabTemp != null)
        {
            return prefabTemp.gameObject;
        }
        else
        {
            return null;
        }
    }
    public GameObject GetPrefab(EPrefabType _prefabType, Vector3 _pos, Quaternion _rotation)
    {
        GameObject goTemp = this.GetPrefab(_prefabType);
        goTemp.transform.position = _pos;
        goTemp.transform.rotation = _rotation;
        return goTemp;
    }
    public GameObject GetPrefab(EPrefabType _prefabType, Vector3 _pos, Vector3 _eulerAngles)
    {
        GameObject goTemp = this.GetPrefab(_prefabType);
        goTemp.transform.position = _pos;
        goTemp.transform.eulerAngles = _eulerAngles;
        return goTemp;
    }

    public void BackToPool(PrefabScript _prefabScript)
    {
        if (!_prefabScript.gameObject.activeInHierarchy)
        {//2019/08/17先用此方法避免已經在列表中的東西重新編入，未來應該會加入掃描重複並排出的function，並且定期呼叫(每一frame都呼叫太耗能)
            Debug.Log("此物件已處關閉狀態，估計本來就在列表中" + _prefabScript.gameObject.GetInstanceID() );
            return;
        }         
        lock (g_poolLock)
        {
            _prefabScript.gameObject.SetActive(false);
            switch (_prefabScript.PrefabType)
            {
                case EPrefabType.BULLET:
                    this.m_listBulletPrefab.Add(_prefabScript);                    
                    break;
                case EPrefabType.BURST_SPARK:
                    this.m_listBurstSparkPrefab.Add(_prefabScript);
                    break;
                case EPrefabType.ENEMY:
                    this.m_listEnemyPrefab.Add(_prefabScript);
                    break;
                case EPrefabType.PICKUP:
                    this.m_listPickupPrefab.Add(_prefabScript);
                    break;
                case EPrefabType.CHARGE_UP_PARTICLE:
                    this.m_listChargeUpParticlePrefab.Add(_prefabScript);
                    break;
                case EPrefabType.WARHEAD:
                    this.m_listWarheadPrefab.Add(_prefabScript);
                    break;
                case EPrefabType.WARNING_EFFECT:
                    this.m_listWarningEffectPrfab.Add(_prefabScript);
                    break;
                case EPrefabType.DRUNKARD:
                    this.m_listDrunkardPrefab.Add(_prefabScript);
                    break;
                case EPrefabType.HP_PICKUP:
                    this.m_listHealthChargePrefab.Add(_prefabScript);
                    break;
                case EPrefabType.GAIN_HP_PARTICLE:
                    this.m_listHealthUpParticlePrefab.Add(_prefabScript);
                    break;
                case EPrefabType.CAN_FOOD_BUFF_PARTICLE:
                    this.m_listCanFoodBuffParticlePrefab.Add(_prefabScript);
                    break;
                case EPrefabType.SHOCKER:
                    this.m_listShockerPrefab.Add(_prefabScript);
                    break;
                case EPrefabType.POWER_DOWN_PARTICLE:
                    this.m_listPowerDownParticlePrefab.Add(_prefabScript);
                    break;
                case EPrefabType.RAVAGER:
                    this.m_listRavagerPrefab.Add(_prefabScript);
                    break;
                default:
                    Debug.Log("無此類別於物件池中，直接刪除");
                    Destroy(_prefabScript.gameObject);
                    break;
            }
        }        
    }
}
