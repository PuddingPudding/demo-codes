﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public struct SPUIChanges
{
    public ESpecialTowerChange change;
    /// <summary>
    /// 觀測物，先未定型別，需要外界傳入參照(例如不能直接傳float，要傳float的指標)
    /// </summary>
    public object observeObj;
    public SPUIChanges(ESpecialTowerChange _change, object _observeTarget)
    {
        change = _change;
        observeObj = _observeTarget;
    }
}

/// <summary>
/// 根據不同防禦塔，可能需要調用特殊的UI資源，但因為不是全塔通用，所以特別用個Enum去做標記
/// </summary>
public enum ESpecialTowerChange
{
    COMBO_ATK,
    ULT_2IN1,
    AUTO_SUPPLY,
    CAN_FOOD_BUFF,
    OCL_FAST_RELOAD,
    WEST_DYNAMITE_UI,
    WEST_SHOTGUN
}

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private TowerMainScript m_mainPlayer;
    [SerializeField]
    private Image m_HPBar;
    [SerializeField]
    private Text m_HPText;
    [SerializeField]
    private string m_sFrontText = "HP:";
    [SerializeField]
    private Image m_ammoBar;
    [SerializeField]
    private Text m_ammoText;
    [SerializeField]
    private string m_sAmmoFrontText = "Ammo:";
    [SerializeField]
    private GameObject m_reloadEffect;
    [SerializeField]
    private AudioSource m_reloadAudioSrc;
    [SerializeField]
    private AudioEvent m_reloadSoundEffent; //2019/12/10 之後改將換彈音效搬到玩家身上，估計之後這裡就不用了
    [SerializeField]
    private Image m_reloadArcLeft;
    [SerializeField]
    private Image m_reloadArcRight;
    [SerializeField]
    private float m_fReloadEffectDuration = 0.5f;
    private Camera m_mainCam;
    [SerializeField]
    private float m_fCamShakeDuration = 0.5f;
    [SerializeField]
    private float m_fCamShakeForce = 0.1f;
    [SerializeField]
    private Color m_alertColor = Color.red;
    [SerializeField]
    private Color m_originColor = Color.green;
    [SerializeField]
    private float m_fAlertTime = 0.4f;
    [SerializeField]
    private float m_fAlrRecoverTime = 1.2f;
    [SerializeField]
    private Image m_ultChargeFill;
    [SerializeField]
    private GameObject m_ultChargeShiningEffect;
    [SerializeField]
    private Text m_ultText;
    [SerializeField]
    private string m_sUltFrontText = "Charge:";
    [SerializeField]
    private string m_sUltReadyText = "Ready!!";
    [SerializeField]
    private Image m_ultDurationBar;
    [SerializeField]
    private Text m_ultDurationText;
    [SerializeField]
    private Animator m_ultDurationAnim;
    [SerializeField]
    private RotateBtn m_leftBtn;
    [SerializeField]
    private RotateBtn m_rightBtn;
    [SerializeField]
    private JoystickMover m_joystick;
    [SerializeField]
    private CombatUIScript m_combatGroup;
    [SerializeField]
    private Button m_ultBtn2In1;
    [Header("以下是需要時才生成的prefab")]
    [SerializeField]
    private ComboScript m_comboBarPrefab;
    [SerializeField]
    private AutoSupplyUIScript m_autoSupplyPrefab;
    [SerializeField]
    private CanFoodUI m_canFoodPrefab;
    [SerializeField]
    private OclFastReloadUI m_oclFastReloadPrefab;
    [SerializeField]
    private Vector2 m_dynamiteBtnPos;
    [SerializeField]
    private Button m_dynamiteBtn;
    [SerializeField]
    private WesternSPUI m_westernSPUI;

    private bool m_bReloadEffectTweening = false; //填彈效果是否撥放中
    private float m_fPlayerOriginHP = 0;//玩家原本的血量，用來比對說變化後是增加還是減少
    private bool m_bUlt2In1 = false; //玩家是否選了大招二合一

    private List<Vector3> m_listUltBtnOriginPos = new List<Vector3>();
    private List<SPUIChanges> m_listSPUIChanges = new List<SPUIChanges>();

    public void Init()
    {
        m_mainPlayer.AddModifyNotice(this.PlayerHPModify, EPlayerModifyType.OnHPModify);
        m_mainPlayer.AddModifyNotice(this.PlayerAmmoModify, EPlayerModifyType.OnAmmoModify);
        m_mainPlayer.AddModifyNotice(this.PlayerReloadCtrModify, EPlayerModifyType.OnReloadCtrModify);
        m_mainPlayer.AddModifyNotice(this.PlayerReloadingEffect, EPlayerModifyType.OnReloadStateModify);
        m_mainPlayer.AddModifyNotice(this.PlayerUltChargeModify, EPlayerModifyType.OnChargeNumModify);
        m_mainPlayer.AddModifyNotice(this.PlayerUltDurationModify, EPlayerModifyType.OnUltDurationModify);
        m_mainPlayer.AddModifyNotice(this.PlayerUltCostModify, EPlayerModifyType.OnUltCostModify);
        SettingsManager.Instance.AddControlModeObserver(this.SwitchControlMode);
        this.SwitchControlMode(SettingsManager.Instance.Details.ControlMode);
        this.m_originColor = m_HPBar.color;

        m_mainPlayer.TowerSPchangeNotice += this.OnPlayerSpecialChange;
    }
    public void SetMainPlayer(TowerMainScript _mainPlayer)
    {
        this.m_mainPlayer = _mainPlayer;
        this.Init();
        this.PlayerHPModify();
        this.PlayerAmmoModify();
        //兩組操作UI的設定
        m_leftBtn.SetPlayer(m_mainPlayer);
        m_rightBtn.SetPlayer(m_mainPlayer);
        m_joystick.SetPlayer(m_mainPlayer);
        m_combatGroup.SetPlayer(m_mainPlayer);

        ESpecialTowerChange[] arrDefaultChanges = m_mainPlayer.GetDefaultSPChange();
        for (int i = 0; i < arrDefaultChanges.Length; i++)
        { //檢測說這座防禦塔是否有預設的特殊變化(如山貓塔的罐罐UI)，有的話就全部都先生成出來
            this.OnPlayerSpecialChange(arrDefaultChanges[i]);
        }
    }
    private void OnDisable()
    {
        SettingsManager.Instance.RemoveControlModeObserver(this.SwitchControlMode);
    }

    private void Start()
    {
        this.m_mainCam = Camera.main;
        this.m_ultChargeShiningEffect.SetActive(false);
        m_bReloadEffectTweening = false; //填彈效果是否撥放中
        m_fPlayerOriginHP = 0;//玩家原本的血量，用來比對說變化後是增加還是減少
        m_bUlt2In1 = false; //玩家是否選了大招二合一
    }

    private void PlayerHPModify()
    {
        this.m_HPBar.fillAmount = (m_mainPlayer.HP / m_mainPlayer.MaxHP);
        this.m_HPText.text = m_sFrontText + m_mainPlayer.HP;
        if (m_mainPlayer.HP < m_fPlayerOriginHP)
        {
            this.m_mainCam.DOShakePosition(m_fCamShakeDuration, m_fCamShakeForce);
            this.m_HPBar.DOKill(false);
            this.m_HPBar.DOColor(this.m_alertColor, this.m_fAlertTime).OnComplete(() =>
            {
                this.m_HPBar.DOColor(this.m_originColor, this.m_fAlrRecoverTime);
            });
        }
        m_fPlayerOriginHP = m_mainPlayer.HP;
    }
    private void PlayerAmmoModify()
    {
        this.m_ammoBar.fillAmount = ((float)m_mainPlayer.Ammo / m_mainPlayer.MaxAmmo);
        this.m_ammoText.text = m_sAmmoFrontText + m_mainPlayer.Ammo;
    }
    private void PlayerReloadCtrModify()
    {
        this.m_reloadArcLeft.fillAmount = (m_mainPlayer.ReloadCtr / m_mainPlayer.ReloadTime);
        this.m_reloadArcRight.fillAmount = (m_mainPlayer.ReloadCtr / m_mainPlayer.ReloadTime);
    }

    private void PlayerReloadingEffect()
    {
        if (m_mainPlayer.IsReloading)
        {
            this.m_reloadEffect.transform.DOKill();
            this.m_bReloadEffectTweening = true;
            this.m_reloadEffect.transform.DOScale(1, m_fReloadEffectDuration);
            this.m_reloadEffect.transform.DORotate(Vector3.zero, m_fReloadEffectDuration).OnComplete(() =>
            {
                this.m_bReloadEffectTweening = false;
            });
        }
        else if (!m_mainPlayer.IsReloading)
        {
            this.m_reloadEffect.transform.DOKill();
            this.m_bReloadEffectTweening = true;
            this.m_reloadEffect.transform.DOScale(0, m_fReloadEffectDuration);
            this.m_reloadEffect.transform.DORotate(new Vector3(0, 0, 90), m_fReloadEffectDuration).OnComplete(() =>
          {
              this.m_bReloadEffectTweening = false;
          });
        }
    }

    private void PlayerUltChargeModify()
    {
        if (m_mainPlayer.CurrentCharges < m_mainPlayer.ChargesNeed)
        {
            this.m_ultChargeShiningEffect.SetActive(false);
            this.m_ultChargeFill.fillAmount = ((float)m_mainPlayer.CurrentCharges / m_mainPlayer.ChargesNeed);
            this.m_ultText.text = m_sUltFrontText + "(" + m_mainPlayer.CurrentCharges + "/" + m_mainPlayer.ChargesNeed + ")";
            if (m_combatGroup.UltBtnReady)
            {
                this.PlayerUltReady(false);
            }
        }
        else
        {
            this.m_ultChargeFill.fillAmount = 1;
            this.m_ultText.text = m_sUltReadyText + "+(" + (m_mainPlayer.CurrentCharges - m_mainPlayer.ChargesNeed) + "/" + m_mainPlayer.ChargesMaxBackup + ")";
            this.m_ultChargeShiningEffect.SetActive(true);
            if (!m_combatGroup.UltBtnReady)
            {
                this.PlayerUltReady(true);
            }
        }
    }

    private void PlayerUltReady(bool _bFlag)
    {
        this.PlayerUltCostModify();
        this.m_combatGroup.ShowUltBtns(_bFlag, m_bUlt2In1);
    }

    private void PlayerUltDurationModify()
    {
        this.m_ultDurationAnim.SetFloat(EUltDurationBarAnimParam.ULT_DURATION, m_mainPlayer.UltDuration);
        this.m_ultDurationBar.fillAmount = (m_mainPlayer.UltDuration / m_mainPlayer.UltDurationMax);
        this.m_ultDurationText.text = "Duration:" + m_mainPlayer.UltDuration.ToString("0.0");
    }
    /// <summary>
    /// 去檢查玩家每個大招各自的確切消耗
    /// </summary>
    private void PlayerUltCostModify()
    {
        this.m_combatGroup.SetUltNeed(m_mainPlayer.ChargesNeed, m_mainPlayer.U1ActualChargesNeed
            , m_mainPlayer.U2ActualChargesNeed, m_mainPlayer.U2In1ActualChargesNeed);
    }

    private void SwitchControlMode(EControlMode _mode)
    {
        switch (_mode)
        {
            case EControlMode.BUTTON:
                m_rightBtn.gameObject.SetActive(true);
                m_leftBtn.gameObject.SetActive(true);
                m_joystick.gameObject.SetActive(false);
                break;
            case EControlMode.FREE_JOYSTICK:
                m_rightBtn.gameObject.SetActive(false);
                m_leftBtn.gameObject.SetActive(false);
                m_joystick.gameObject.SetActive(true);
                break;
        }
    }

    private void OnPlayerSpecialChange(ESpecialTowerChange _change)
    {
        Debug.Log("特殊的UI變動，參數為:" + _change);
        switch (_change)
        {
            case ESpecialTowerChange.COMBO_ATK:
                m_mainPlayer.TowerSPFloatNotice += OnComboChange;
                m_comboBarPrefab = GameObject.Instantiate(m_comboBarPrefab.gameObject).GetComponent<ComboScript>();
                m_comboBarPrefab.transform.SetParent(this.transform);
                m_comboBarPrefab.transform.localPosition = Vector2.zero;
                m_comboBarPrefab.transform.localScale = Vector3.one;
                break;
            case ESpecialTowerChange.ULT_2IN1:
                m_bUlt2In1 = true;
                m_combatGroup.SwitchToUlt2In1();
                break;
            case ESpecialTowerChange.AUTO_SUPPLY:
                m_mainPlayer.TowerSPFloatNotice += OnAutoSupply;
                m_autoSupplyPrefab = GameObject.Instantiate(m_autoSupplyPrefab.gameObject).GetComponent<AutoSupplyUIScript>();
                m_autoSupplyPrefab.transform.SetParent(this.transform);
                m_autoSupplyPrefab.transform.localPosition = Vector2.zero;
                m_autoSupplyPrefab.transform.localScale = Vector3.one;
                break;
            case ESpecialTowerChange.CAN_FOOD_BUFF:
                m_canFoodPrefab = GameObject.Instantiate(m_canFoodPrefab.gameObject).GetComponent<CanFoodUI>();
                m_canFoodPrefab.transform.SetParent(this.transform);
                m_canFoodPrefab.transform.localScale = Vector3.one;
                m_canFoodPrefab.transform.localPosition = Vector2.zero;
                m_mainPlayer.AddModifyNotice(this.SwitchCanFoodBuff, EPlayerModifyType.OnReloadStateModify);
                m_mainPlayer.AddModifyNotice(this.ModifyCanFoodReload, EPlayerModifyType.OnReloadCtrModify);
                m_mainPlayer.TowerSPFloatNotice += this.ModifyCanFoodBuffDuration;
                m_mainPlayer.TowerSPBoolNotice += this.ModifyCanFoodBuffState;
                break;
            case ESpecialTowerChange.OCL_FAST_RELOAD:
                m_oclFastReloadPrefab = GameObject.Instantiate(m_oclFastReloadPrefab.gameObject).GetComponent<OclFastReloadUI>();
                m_oclFastReloadPrefab.transform.SetParent(this.transform);
                m_oclFastReloadPrefab.transform.localScale = Vector3.one;
                m_oclFastReloadPrefab.transform.localPosition = Vector3.zero;
                m_mainPlayer.AddModifyNotice(this.ModifyOclFastReloadTriggered, EPlayerModifyType.OnReloadStateModify);
                m_mainPlayer.TowerSPFloatNotice += this.ModifyOclFastReloadProgress;
                break;
            case ESpecialTowerChange.WEST_DYNAMITE_UI:
                #region 設置西部塔的特殊UI
                m_westernSPUI = GameObject.Instantiate(m_westernSPUI, this.transform);
                m_westernSPUI.transform.localScale = Vector3.one;
                m_westernSPUI.transform.localPosition = Vector3.zero;
                m_mainPlayer.TowerSPIntNotice += this.ModifyWesternDynamiteNum;
                m_mainPlayer.TowerSPFloatNotice += this.ModifyWesternDynamiteProgressBar;
                #endregion 設置西部塔的特殊UI
                #region 設置西部塔的炸藥按鈕
                this.m_dynamiteBtn = GameObject.Instantiate(m_dynamiteBtn, this.transform);
                this.m_dynamiteBtn.transform.localPosition = m_dynamiteBtnPos;
                WesternTowerMainScript westernTemp = (WesternTowerMainScript)m_mainPlayer;
                this.m_dynamiteBtn.onClick.AddListener(westernTemp.UseDynamite);
                #endregion 設置西部塔的炸藥按鈕
                break;
            case ESpecialTowerChange.WEST_SHOTGUN:
                m_mainPlayer.TowerSPIntNotice += this.ModifyWesternShotgunUI;
                break;
        }
    }
    private void OnComboChange(ESpecialTowerChange _change, float _fValue)
    {
        if (_change == ESpecialTowerChange.COMBO_ATK)
        {
            m_comboBarPrefab.FillAmount = _fValue;
        }
    }
    private void OnAutoSupply(ESpecialTowerChange _change, float _fValue)
    {
        if (_change == ESpecialTowerChange.AUTO_SUPPLY)
        {
            m_autoSupplyPrefab.FillAmount = _fValue;
        }
    }
    private void SwitchCanFoodBuff()
    {
        if (m_mainPlayer.IsReloading)
        {
            m_canFoodPrefab.SwitchBuffing(false);
        }
    }
    private void ModifyCanFoodBuffDuration(ESpecialTowerChange _change, float _fValue)
    {
        if (_change == ESpecialTowerChange.CAN_FOOD_BUFF)
        {
            m_canFoodPrefab.ModifyBuffUI(_fValue);
        }
    }
    private void ModifyCanFoodBuffState(ESpecialTowerChange _change, bool _bFlag)
    {
        if (_change == ESpecialTowerChange.CAN_FOOD_BUFF)
        {
            m_canFoodPrefab.SwitchBuffing(_bFlag);
        }
    }
    private void ModifyCanFoodReload()
    {
        if (!m_canFoodPrefab.m_bBuffing)
        {
            m_canFoodPrefab.ModifyBuffUI(m_mainPlayer.ReloadCtr / m_mainPlayer.ReloadTime);
        }
    }
    private void ModifyOclFastReloadProgress(ESpecialTowerChange _change, float _value)
    {
        if (_change == ESpecialTowerChange.OCL_FAST_RELOAD)
        {
            m_oclFastReloadPrefab.FillProgress = _value;
        }
    }
    private void ModifyOclFastReloadTriggered()
    {
        if (m_mainPlayer.IsReloading) { m_oclFastReloadPrefab.StartFastReload(); }
    }
    private void ModifyWesternDynamiteNum(ESpecialTowerChange _change, int _iNum)
    {
        if (_change == ESpecialTowerChange.WEST_DYNAMITE_UI) { m_westernSPUI.SetDynamiteNum(_iNum); }
    }
    private void ModifyWesternDynamiteProgressBar(ESpecialTowerChange _change, float _fValue)
    {
        if (_change == ESpecialTowerChange.WEST_DYNAMITE_UI) { m_westernSPUI.SetDynamiteProgress(_fValue); }
    }
    private void ModifyWesternShotgunUI(ESpecialTowerChange _change, int _iNum)
    {
        if (_change == ESpecialTowerChange.WEST_SHOTGUN) { m_westernSPUI.SetShotgunAmmo(_iNum); }
    }
}
