﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class ScanControllerDemo : MonoBehaviour
{
	[SerializeField]
	private float m_fDistortionTime = 0.2f;

	[SerializeField]
	private float m_fMaxIntansity = 80;

	[SerializeField]
	private PostProcessVolume m_ppv;

	[SerializeField]
	private int m_iGridNum; //每一邊會有的格子個數

	[SerializeField]
	private float m_fGridSize;//格子的大小

	[SerializeField]
	private Transform m_playerTransform; //玩家的Transform(每次網格浮現時，都會先去找玩家的位置定位)

	[SerializeField]
	private Material m_gridMat; //網格的材質

	[SerializeField]
	private float m_fGridFadeTime = 0.6f;

	[SerializeField]
	private float m_fGridMaxAlpha = 0.6f;

	[SerializeField]
	private GameObject m_gridDot; //網格邊角的小點點

	[SerializeField]
	private Material m_demoMat;

	[SerializeField]
	private Material m_scannerMat;

	[SerializeField]
	private Camera m_cam;

	private LensDistortion m_lensDistortion = null; //你無法直接在Inspector那邊把Profile的後製特效拉進來
	private DepthOfField m_depthOfField = null; //所以這裡就不用SerializeField了
	private IEnumerator m_coroutineHandler = null;
	private IEnumerator m_gridCoroutineHandler = null;
	private float m_fOriginFocusDistance = 10;
	private Vector3 m_gridCenter = Vector3.zero; //網格的中心點
	private List<GameObject> m_listGridDot = new List<GameObject>();

	private void Start()
	{
		m_lensDistortion = m_ppv.profile.GetSetting<LensDistortion>();
		m_depthOfField = m_ppv.profile.GetSetting<DepthOfField>();
		m_fOriginFocusDistance = m_depthOfField.focusDistance.value;
		m_gridCenter = m_playerTransform.position;
		m_gridMat.SetFloat("_Alpha", 0);
		this.ShowGridDots();

		m_scannerMat.SetFloat("_CamFarClip" , m_cam.farClipPlane);
		m_scannerMat.SetVector("_WorldSpaceScannerPos", m_playerTransform.position); //設定掃描點位置(玩家)
	}

	private void OnPostRender()//該函式會在每次Camera渲染完畢後呼叫
	{//特別注意:該函式只有掛在Camera上時才會生效
		this.DrawMatrixLines();
	}

	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		this.ScannerBlit(source, destination, m_scannerMat);
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.E))
		{
			if (this.m_coroutineHandler != null) { StopCoroutine(m_coroutineHandler); }
			//中斷正在進行的coroutine
			this.m_coroutineHandler = this.DistortionCoroutine();
			StartCoroutine(this.m_coroutineHandler);
		}
	}

	IEnumerator DistortionCoroutine()
	{
		float fDistortionCtr = 0;
		this.m_depthOfField.focusDistance.value = 0.1f; //將焦距設到最小值
		this.m_lensDistortion.intensity.value = this.m_fMaxIntansity;
		this.m_gridMat.SetFloat("_Alpha", 0); //模糊前先將網格材質的Alpha歸零(直接打Shader那邊宣告的變數名稱)
		this.m_gridCenter = m_playerTransform.transform.position;
		this.ShowGridDots(); //每次重新演示模糊特效都順便去把網格中心重設

		while (fDistortionCtr <= m_fDistortionTime)
		{
			yield return null;
			fDistortionCtr += Time.deltaTime;
			float fProgress = fDistortionCtr / m_fDistortionTime;//計算現在特效的進度百分比
			this.m_depthOfField.focusDistance.value = Mathf.Lerp(0.1f, m_fOriginFocusDistance, fProgress);
			this.m_lensDistortion.intensity.value = Mathf.Lerp(m_fMaxIntansity, 0, fProgress);
		}
		this.m_depthOfField.focusDistance.value = m_fOriginFocusDistance;
		this.m_lensDistortion.intensity.value = 0; //為了避免最後有誤差直接設定

		float fFadeCtr = 0;
		while (fFadeCtr <= m_fGridFadeTime)
		{
			yield return null;
			fFadeCtr += Time.deltaTime;
			float fProgress = fFadeCtr / m_fGridFadeTime;
			this.m_gridMat.SetFloat("_Alpha", Mathf.Lerp(0, m_fGridMaxAlpha, fProgress));
		}
		this.m_gridMat.SetFloat("_Alpha", m_fGridMaxAlpha);
	}

	private void DrawMatrixLines()
	{
		float fOffset = ((float)m_iGridNum / 2) * m_fGridSize;
		Vector3 showPosStart = m_gridCenter - new Vector3(fOffset, fOffset, fOffset); //算出顯示的起點
		for (int i = 0; i <= m_iGridNum; i++)
		{
			for (int j = 0; j <= m_iGridNum; j++)
			{
				Vector3 startPos = showPosStart + new Vector3(0, i * m_fGridSize, j * m_fGridSize);
				Vector3 endPos = showPosStart + new Vector3(fOffset * 2, i * m_fGridSize, j * m_fGridSize);
				this.DrawLine(startPos, endPos); //繪製X軸(左右)線條

				startPos = showPosStart + new Vector3(i * m_fGridSize, j * m_fGridSize, 0);
				endPos = showPosStart + new Vector3(i * m_fGridSize, j * m_fGridSize, fOffset * 2);
				this.DrawLine(startPos, endPos); //繪製Z軸(前後)的線條

				startPos = showPosStart + new Vector3(i * m_fGridSize, 0, j * m_fGridSize);
				endPos = showPosStart + new Vector3(i * m_fGridSize, fOffset * 2, j * m_fGridSize);
				this.DrawLine(startPos, endPos);//繪製Y軸(上下)的線條
			}
		}
	}
	private void DrawLine(Vector3 _start, Vector3 _end)
	{
		GL.PushMatrix();
		m_gridMat.SetPass(0);//將該Material的第1個Pass(渲染方式)帶入當前渲染模式之中
		GL.Begin(GL.LINES); //叫GL開始繪製線條
		GL.Vertex(_start); //設置節點，GL將依序繪製
		GL.Vertex(_end);
		GL.End();
		GL.PopMatrix();
	}

	private void ShowGridDots() //顯示點點
	{
		float fOffset = ((float)m_iGridNum / 2) * m_fGridSize;
		Vector3 showPosStart = m_gridCenter - new Vector3(fOffset, fOffset, fOffset);
		int iMaxSpawn = (m_iGridNum + 1) * (m_iGridNum + 1) * (m_iGridNum + 1);
		int iDotCtr = 0;
		for (int i = 0; i <= m_iGridNum; i++)
		{
			for (int j = 0; j <= m_iGridNum; j++)
			{
				for (int k = 0; k <= m_iGridNum; k++)
				{
					Vector3 spawnPos = showPosStart + new Vector3(i * m_fGridSize, j * m_fGridSize, k * m_fGridSize);
					if (m_listGridDot.Count < iMaxSpawn)//若還未存有足夠的點點，那就在此補生成
					{
						GameObject dotTemp = Instantiate(m_gridDot);
						m_listGridDot.Add(dotTemp);
					}
					m_listGridDot[iDotCtr].transform.position = spawnPos;
					iDotCtr++;
				}
			}
		}
	}

	private void ScannerBlit(RenderTexture source, RenderTexture dest, Material mat)
	{
		RenderTexture.active = dest; //設定現在被渲染的RenderTexture，這裡設為輸出的那一個
		mat.SetTexture("_MainTex", source); //將原本的畫面截圖傳進Shader當中

		float camFar = m_cam.farClipPlane;
		float camFOV = m_cam.fieldOfView; //取得Camera的視線角度 (最高到179)
		float fovHalf = camFOV * 0.5f; //取得視線角度的一半 (為了要算邊邊的向量)
		float camAspect = m_cam.aspect; //取得Camera的畫面比例 (寬/高)

		Vector3 toRight = m_cam.transform.right * Mathf.Tan(fovHalf * Mathf.Deg2Rad) * camAspect;
		//Camera視線範圍朝右的基本向量……我不太會形容
		Vector3 toTop = m_cam.transform.up * Mathf.Tan(fovHalf * Mathf.Deg2Rad);
		//Camera視線範圍朝上的基本向量
		Vector3 topLeft = (m_cam.transform.forward - toRight + toTop);
		//Camera視線範圍朝左上的基本向量
		float camScale = topLeft.magnitude * camFar;
		//將最遠可視距離乘上往左上之基本向量的長度，得出Camera四個角落打出去的最遠可視距離
		topLeft.Normalize();
		topLeft *= camScale; //將左上基本向量單位化(長度變1)，然後再乘以邊角向量的長度

		Vector3 topRight = (m_cam.transform.forward + toRight + toTop);
		topRight.Normalize();
		topRight *= camScale;

		Vector3 bottomRight = (m_cam.transform.forward + toRight - toTop);
		bottomRight.Normalize();
		bottomRight *= camScale;

		Vector3 bottomLeft = (m_cam.transform.forward - toRight - toTop);
		bottomLeft.Normalize();
		bottomLeft *= camScale;

		GL.PushMatrix(); //儲存模型，投影矩陣等資訊……我也看不太懂
		GL.LoadOrtho(); //帶入正投影(orthograhic projection)矩陣，看不太懂，但是拿掉這一行的話就無法正常顯示

		mat.SetPass(0); //將帶入的材質設為當前繪製用的材質
		GL.Begin(GL.QUADS); //請GL開始繪製四邊形

		GL.MultiTexCoord2(0, 0.0f, 0.0f);//設定Shader當中的第一組紋理座標(TEXCOORD0)，一般來說第一組都是用來表示UV座標
		GL.MultiTexCoord(1, bottomLeft); //設定第二組紋理座標(TEXCOORD1)，設定左下角看出去到底的向量，中間部分Shader會取插值
		GL.Vertex3(0.0f, 0.0f, 0.0f); //從0.0(畫面左下角開始繪製)

		GL.MultiTexCoord2(0, 1.0f, 0.0f);
		GL.MultiTexCoord(1, bottomRight);
		GL.Vertex3(1.0f, 0.0f, 0.0f);

		GL.MultiTexCoord2(0, 1.0f, 1.0f);
		GL.MultiTexCoord(1, topRight);
		GL.Vertex3(1.0f, 1.0f, 0.0f);

		GL.MultiTexCoord2(0, 0.0f, 1.0f);
		GL.MultiTexCoord(1, topLeft);
		GL.Vertex3(0.0f, 1.0f, 0.0f); //一路設定到0,1 (畫面左上角)

		GL.End(); //GL繪製結束
		GL.PopMatrix(); //取出先前儲存的模型與投影矩陣等資訊
	}
}