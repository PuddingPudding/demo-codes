﻿Shader "Demo/Blit Shader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {} //宣告該Shader的主要貼圖，這邊的話我們將會把畫面渲染結果帶入(有點像把當下的截圖傳進來)
		_Brightness("Brightness" , Range(0, 2)) = 1 //宣告亮度
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

			float _Brightness; //記得要在SubShader當中再次宣告你要用的變數

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);

				col *= _Brightness; //將傳進來的截圖顏色乘上亮度，數字越高越接近白色(亮)，反之則越暗

                return col;
            }
            ENDCG
        }
    }
}
