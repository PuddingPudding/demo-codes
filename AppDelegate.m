#import "AppDelegate.h"

#include "ViewController.h" //記得要把ViewController的標頭檔引入

@interface AppDelegate ()

@end

//一些全域變數的宣告，擷取自官方教學 (MainViewController.mm)
int gArgc = 0;
char** gArgv = NULL;
NSDictionary* appLaunchOpts; //iOS當中用以表示App為何被開啟，有時會是空的
//宣告一些呼叫Unity需要的函式，擷取自官方教學 (MainViewController.mm)
UnityFramework* UnityFrameworkLoad()//初始化Unity並將其回傳
{
    NSString* bundlePath = nil;
    bundlePath = [[NSBundle mainBundle] bundlePath];
    bundlePath = [bundlePath stringByAppendingString: @"/Frameworks/UnityFramework.framework"];
    
    NSBundle* bundle = [NSBundle bundleWithPath: bundlePath];
    if ([bundle isLoaded] == false) [bundle load];
    
    UnityFramework* ufw = [bundle.principalClass getInstance];
    if (![ufw appController])
    {
        // unity is not initialized
        [ufw setExecuteHeader: &_mh_execute_header];
    }
    return ufw;
}

@implementation AppDelegate

//為了生成我們的iOS原生視窗，這裡要去覆寫原本的didFinishLaunchingWithOptions
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    appLaunchOpts = launchOptions;//紀錄app的啟動原因
    
    self.nativeWindow = [[UIWindow alloc] initWithFrame: [UIScreen mainScreen].bounds];//初始化視窗並設定大小為填滿畫面
    self.viewController = [[ViewController alloc] init]; //初始化視窗的邏輯 (包含你自己宣告的UI元素）
    self.navVC = [[UINavigationController alloc] initWithRootViewController: self.viewController];//初始化視窗切換控制器並設定你自己的視窗為根視窗
    self.nativeWindow.rootViewController = self.navVC; //將視窗切換控制器指派給自己的原生視窗
    [self.nativeWindow makeKeyAndVisible]; //顯示自己的原生視窗
    NSLog(@"原生App初始化");
    return YES;
}

//宣告一些呼叫Unity需要的函式
- (bool)unityIsInitialized { return [self ufw] && [[self ufw] appController]; }//檢查Unity是否已做初始化（看是否為Null)

//實作那些AppDelegate.h當中宣告的函式
- (void)initUnity
{
    NSLog(@"試著進行Unity初始化");
    if([self unityIsInitialized])
    {
        NSLog(@"Unity已初始化，因此該呼叫無效");
        return;
    }
    
    [self setUfw: UnityFrameworkLoad()]; //XCode當中，變數會自己產生set變數名稱這樣的setter
    [[self ufw] setDataBundleId: "com.unity3d.framework"];
    [[self ufw] registerFrameworkListener: self]; //註冊Unity的事件聆聽者，確保在Unity部份動作後原生iOS能有回應
    [NSClassFromString(@"NativeCallMgr") registerNativeCaller :self]; //註冊原生呼叫（先前的NativeCallForUnity.h文檔中），讓Unity可以呼叫iOS
    [[self ufw] runEmbeddedWithArgc: gArgc argv: gArgv appLaunchOpts: appLaunchOpts];

    [[self ufw] showUnityWindow];
}
- (void)showUnity
{
    if(![self unityIsInitialized]) {
        NSLog(@"Unity尚未初始化");
    } else {
        NSLog(@"試著顯示Unity");
        [[self ufw] showUnityWindow];
    }
}
//以下實作用來跟Unity溝通的函式
- (void) receiveMessageFromUnity:(char *)msg //發現NSString會失敗，因此改用char
{
    NSLog(@"Unity傳來的訊息為 %s" , msg);
}
//- (NSString *) getMessageFromiOS
- (char *) getMessageFromiOS
{
//    NSString *outputStr = @"iOS說你好"; //上面的是NSString的做法，後來發現行不通
    char* outputStr = (char*)malloc(strlen("iOS發現你")+1); //你得要分配字串+1的大小空間，因為字串結尾還會帶有一個結束字元
    strcpy(outputStr , "iOS發現你");
    
    NSLog(@"給Unity的字串 %s" , outputStr);
    return outputStr;
}
- (void) turnOffUnity
{
    [UnityFrameworkLoad() unloadApplication];
}

//以下實作UnityFrameworkListener原本要求的事項
- (void)unityDidUnload:(NSNotification*)notification
{
    NSLog(@"收到Unity unload通知");
    
    [[self ufw] unregisterFrameworkListener: self];
    [self setUfw: nil];
    [self.nativeWindow makeKeyAndVisible]; //顯示自己的原生視窗
}

#pragma mark - UISceneSession lifecycle //這一行有點像是程式碼的書籤，不過我本身不太會用

//移除下方原本為application的段落
//- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options {
//    // Called when a new scene session is being created.
//    // Use this method to select a configuration to create the new scene with.
//    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
//}
//
//
//- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions {
//    // Called when the user discards a scene session.
//    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
//    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
//}


@end


int main(int argc, char* argv[]) //將原生專案中的main.m拿掉後加入新的int main（取自官方範例）
{
    NSLog(@"int main 呼叫");
    gArgc = argc;
    gArgv = argv;
    
    @autoreleasepool
    {
        if (false)
        {
            // run UnityFramework as main app
            id ufw = UnityFrameworkLoad();
            
            // Set UnityFramework target for Unity-iPhone/Data folder to make Data part of a UnityFramework.framework and call to setDataBundleId
            // ODR is not supported in this case, ( if you need embedded and ODR you need to copy data )
            [ufw setDataBundleId: "com.unity3d.framework"];
            [ufw runUIApplicationMainWithArgc: argc argv: argv];
        } else {
            // run host app first and then unity later
            UIApplicationMain(argc, argv, nil, [NSString stringWithUTF8String: "AppDelegate"]);
        }
    }
    
    return 0;
}